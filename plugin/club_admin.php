<?php

class club_admin
{
    public function __construct($file)
    {
        register_activation_hook($file, array('mi_database', 'installAll'));

        register_deactivation_hook($file, array('mi_database', 'freeAll'));

        register_uninstall_hook($file, array('mi_database', 'uninstallAll'));

        add_action('admin_menu', array($this, 'add_menu_back'));

        add_action('widgets_init', array($this, 'registerWidgets'));
    }

    public function registerStyles()
    {
        wp_register_style('mi_style',
            plugins_url('css/mi_style.css', __FILE__));
        wp_enqueue_style('mi_style');
    }

    public function registerWidgets() {
        register_widget('class_wp_mi_club_widget');
        register_widget('class_wp_mi_member_widget');
        register_widget('class_wp_mi_championship_club_widget');
        register_widget('class_wp_mi_championship_member_widget');
    }

    public function add_menu_back()
    {
        ///Club
        $hook = add_menu_page("Les clubs enregistrés", 'Gestionnaire des clubs', 'manage_options', 'club_db', array($this, 'clubfunc'), 'dashicons-groups', 46);
        add_submenu_page("club_db", "Enregistrer un club", "Enregistrer un club", "manage_options", "club_add", array($this, "clubAdder"));
        ///Members
        add_menu_page("Gestionnaire des membres", "Gestionnaire des membres", "manage_options", "club_member_db", array($this, "memberfunc"), 'dashicons-admin-users', 47);
        add_submenu_page("club_member_db", "Enregistrer un membre", "Enregistrer un membre", "manage_options", "club_member_add", array($this, "memberAdder"));
        ///Category
        add_menu_page("Gestionnaire des catégories", "Gestionnaire des catégories", "manage_options", "club_category_db", array($this, "categoryfunc"), 'dashicons-admin-post', 48);
        add_submenu_page("club_category_db", "Enregistrer une catégorie", "Enregistrer une catégorie", "manage_options", "club_category_add", array($this, "categoryAdder"));
        ///Championship
        add_menu_page("Gestionnaire des championnats", "Gestionnaire des championnats", "manage_options", "club_championship_db", array($this, "championshipfunc"), 'dashicons-awards', 49);
        add_submenu_page("club_championship_db", "Enregistrer un championnat", "Enregistrer un championnat", "manage_options", "club_championship_add", array($this, "championshipAdder"));
        ///Stats
        add_menu_page("Statistiques", "Statistiques", "manage_options", "mi_stats", array($this, "statsfunc"), 'dashicons-chart-bar', 50);
    }

    public function clubfunc() {
        if(mi_array_utils::arrayContain($_GET, "profil", true)) {
            club_display::displayProfil($_GET["profil"]);
        }
        else if(mi_array_utils::containKeyPair($_POST, "delete", "type")) {
            club_display::displayDelete($_POST);
        }
        else if(mi_array_utils::containKeyPair($_POST, "kick", "type")) {
            club_display::displayKick($_POST);
        }
        else if(mi_array_utils::containKeyPair($_POST, "kick_championship", "type")) {
            club_display::displayKickChampionships($_POST);
        }
        else if(mi_array_utils::arrayContain($_POST, "conf_edit", true)) {
            club_display::displayConfEdit($_POST['conf_edit'], $_POST);
        }
        else if(mi_array_utils::arrayContain($_GET, "edit", true)) {
            club_display::displayEditor($_GET['edit']);
        }
        else if(mi_array_utils::arrayContain($_GET, "members", true)) {
            club_display::displayMembers($_GET['members']);
        }
        else if(mi_array_utils::arrayContain($_GET, "championship", true)) {
            club_display::displayChampionships($_GET['championship']);
        }
        else {
            club_display::displayTable();
        }
    }

    public function memberfunc() {
        if(mi_array_utils::arrayContain($_GET, "profil", true)) {
            club_member_display::displayProfil($_GET['profil']);
        }
        else if(mi_array_utils::containKeyPair($_POST, "delete", "type")) {
            club_member_display::displayDelete($_POST);
        }
        else if(mi_array_utils::arrayContain($_POST, "conf_edit", true)) {
            club_member_display::displayConfEdit($_POST['conf_edit'], $_POST);
        }
        else if(mi_array_utils::arrayContain($_GET, "edit", true)) {
            club_member_display::displayEditor($_GET['edit']);
        }
        else {
            club_member_display::displayTable();
        }
    }

    public function categoryfunc() {
        if(mi_array_utils::containKeyPair($_POST, "delete", "type")) {
            club_category_display::displayDelete($_POST);
        }
        else if(mi_array_utils::arrayContain($_POST, "conf_edit", true)) {
            club_category_display::displayConfEdit($_POST['conf_edit'], $_POST);
        }
        else if(mi_array_utils::arrayContain($_GET, "edit", true)) {
            club_category_display::displayEditor($_GET['edit']);
        }
        else if(mi_array_utils::arrayContain($_GET, "members", true)) {
            club_category_display::displayMembers($_GET['members']);
        }
        else {
            club_category_display::displayTable();
        }
    }

    public function championshipfunc() {
        if(mi_array_utils::containKeyPair($_POST, "delete", "type")) {
            championship_display::displayDelete($_POST);
        }
        else if(mi_array_utils::containKeyPair($_POST, "kick", "type")) {
            championship_display::displayKick($_POST);
        }
        else if(mi_array_utils::arrayContain($_POST, "conf_edit", true)) {
            championship_display::displayConfEdit($_POST['conf_edit'], $_POST);
        }
        else if(mi_array_utils::arrayContain($_GET, "edit", true)) {
            championship_display::displayEditor($_GET['edit']);
        }
        else if(mi_array_utils::arrayContain($_GET, "switch_status", true)) {
            championship_display::displaySwitchStatus($_GET['switch_status']);
        }
        else if(mi_array_utils::arrayContain($_GET, "update_result", true)) {
            championship_display::displayResultEditor($_GET['update_result']);
        }
        else if(mi_array_utils::arrayContain($_GET, "settings", true)) {
            championship_display::displaySettingsEditor($_GET['settings']);
        }
        else if(mi_array_utils::arrayContain($_GET, "status", true)) {
            championship_display::displayStatus($_GET['status']);
        }
        else if(mi_array_utils::arrayContain($_GET, "members", true)) {
            championship_display::displayMembers($_GET['members']);
        }
        else if(mi_array_utils::arrayContain($_GET, "results", true)) {
            championship_display::displayResults($_GET['results']);
        }
        else if(mi_array_utils::arrayContain($_GET, "d_members", true)) {
            championship_display::displayChampionshipDynamicMembers($_GET['d_members']);
        }
        else if(mi_array_utils::arrayContain($_GET, "d_members_members", true)) {
            championship_display::displayChampionshipDynamicClubMembers($_GET['d_members_members'], 
            (mi_array_utils::arrayContain($_GET, "club", true) ? $_GET['club'] : '0')
            );
        }
        else if(mi_array_utils::arrayContain($_GET, "championship_member_adder", true)) {
            championship_display::displayMemberAdder($_GET['championship_member_adder']);
        }
        else {
            championship_display::displayTable();
        }
    }

    public function statsfunc() {
        stats_display::display();
    }

    public function clubAdder() {
        club_display::displayAdder();
    }

    public function memberAdder() {
        club_member_display::displayAdder();
    }

    public function categoryAdder() {
        club_category_display::displayAdder();
    }

    public function championshipAdder() {
        championship_display::displayAdder();
    }
}
