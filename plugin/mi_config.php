<?php
class mi_config {
    public static $mi_clubs_db_name = "mi_clubs";
    public static $mi_clubs_member_db_name = "mi_clubs_member";
    public static $mi_clubs_category_db_name = "mi_clubs_category";
    public static $mi_clubs_championship_db_name = "mi_clubs_championship";
    public static $mi_clubs_championship_members_db_name = "mi_clubs_championship_members";
    public static $mi_clubs_championship_settings_db_name = "mi_clubs_championship_settings";
    public static $mi_clubs_championship_status_db_name = "mi_clubs_championship_status";
    public static $mi_clubs_championship_stats_db_name = "mi_clubs_championship_stats";
    public static $mi_clubs_championship_results_db_name = "mi_clubs_championship_results";

    public static $mi_championship_settings_c_min = 0;
    public static $mi_championship_settings_c_max = 1000;
    public static $mi_championship_settings_m_min = 0;
    public static $mi_championship_settings_m_max = 1000;
    public static $mi_championship_settings_max_pts_count = 10;

    public static $club_requirements = [
        "nom", "adresse", "email", "tel", "ddp", "cp"
    ];

    public static $club_member_requirements = [
        "nom", "prenom", "email", "tel", "adresse", "club", "adh", "cp"
    ];

    public static $club_category_requirements = [
        "nom"
    ];

    public static $club_championship_requirements = [
        "nom", "category"
    ];

    public static $club_championship_members_requirements = [
        "championship_id", "club_id"
    ];

    public static $club_championship_settings_requirements = [
        "championship_id", "settings"
    ];

    public static $club_championship_status_requirements = [
        "championship_id", "conf" ,"manche", "finish"
    ];

    public static $club_championship_stats_requirements = [
        "championship_id", "member_id" ,"pts"
    ];

    public static $club_championship_results_requirements = [
        "championship_id", "conf", "manche" , "classement"
    ];
}
