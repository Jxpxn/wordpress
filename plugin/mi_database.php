<?php

class mi_database {
    public static function installClubsDB() {
        global $wpdb;
        $wpdb->query("CREATE TABLE IF NOT EXISTS " . $wpdb->prefix.mi_config::$mi_clubs_db_name . " (id INT AUTO_INCREMENT PRIMARY KEY, nom VARCHAR (150) NOT NULL, adresse VARCHAR (200) NOT NULL, email VARCHAR(255) NOT NULL, tel VARCHAR(150) NOT NULL, ddp INT NOT NULL, participant INT NOT NULL, cp INT NOT NULL);");
    }

    public static function installClubsMemberDB() {
        global $wpdb;
        $wpdb->query("CREATE TABLE IF NOT EXISTS " . $wpdb->prefix.mi_config::$mi_clubs_member_db_name . " (id INT AUTO_INCREMENT PRIMARY KEY, nom VARCHAR (150) NOT NULL, prenom VARCHAR (150) NOT NULL, email VARCHAR(255) NOT NULL, tel VARCHAR(150) NOT NULL, adresse VARCHAR(200) NOT NULL, club INT NOT NULL, adh INT NOT NULL, participant INT NOT NULL, cp INT NOT NULL);");
    }

    public static function installClubsCategoryDB() {
        global $wpdb;
        $wpdb->query("CREATE TABLE IF NOT EXISTS " . $wpdb->prefix.mi_config::$mi_clubs_category_db_name . " (id INT AUTO_INCREMENT PRIMARY KEY, nom VARCHAR (150) NOT NULL);");
    }

    public static function installChampionshipDB() {
        global $wpdb;
        $wpdb->query("CREATE TABLE IF NOT EXISTS " . $wpdb->prefix.mi_config::$mi_clubs_championship_db_name . " (id INT AUTO_INCREMENT PRIMARY KEY, nom VARCHAR (150) NOT NULL, category INT NOT NULL);");
    }

    public static function installChampionshipMembersDB() {
        global $wpdb;
        $wpdb->query("CREATE TABLE IF NOT EXISTS " . $wpdb->prefix.mi_config::$mi_clubs_championship_members_db_name . " (id INT AUTO_INCREMENT PRIMARY KEY, championship_id INT NOT NULL, club_id INT NOT NULL);");
    }

    public static function installChampionshipSettingsDB() {
        global $wpdb;
        $wpdb->query("CREATE TABLE IF NOT EXISTS " . $wpdb->prefix.mi_config::$mi_clubs_championship_settings_db_name . " (id INT AUTO_INCREMENT PRIMARY KEY, championship_id INT NOT NULL, settings VARCHAR (1000) NOT NULL);");
    }

    public static function installChampionshipStatusDB() {
        global $wpdb;
        $wpdb->query("CREATE TABLE IF NOT EXISTS " . $wpdb->prefix.mi_config::$mi_clubs_championship_status_db_name . " (id INT AUTO_INCREMENT PRIMARY KEY, championship_id INT NOT NULL, conf INT NOT NULL, manche INT NOT NULL, finish INT NOT NULL);");
    }

    public static function installChampionshipStatsDB() {
        global $wpdb;
        $wpdb->query("CREATE TABLE IF NOT EXISTS " . $wpdb->prefix.mi_config::$mi_clubs_championship_stats_db_name . " (id INT AUTO_INCREMENT PRIMARY KEY, championship_id INT NOT NULL, member_id INT NOT NULL, pts INT NOT NULL);");
    }

    public static function installChampionshipResultsDB() {
        global $wpdb;
        $wpdb->query("CREATE TABLE IF NOT EXISTS " . $wpdb->prefix.mi_config::$mi_clubs_championship_results_db_name . " (id INT AUTO_INCREMENT PRIMARY KEY, championship_id INT NOT NULL, conf INT NOT NULL, manche INT NOT NULL, classement VARCHAR (1000) NOT NULL);");
    }

    public static function freeClubsDB() {
        global $wpdb;
        $wpdb->query("TRUNCATE TABLE " . $wpdb->prefix.mi_config::$mi_clubs_db_name);
    }

    public static function freeClubsCategoryDB() {
        global $wpdb;
        $wpdb->query("TRUNCATE TABLE " . $wpdb->prefix.mi_config::$mi_clubs_category_db_name);
    }

    public static function freeClubsMemberDB() {
        global $wpdb;
        $wpdb->query("TRUNCATE TABLE " . $wpdb->prefix.mi_config::$mi_clubs_member_db_name);
    }

    public static function freeChampionshipDB() {
        global $wpdb;
        $wpdb->query("TRUNCATE TABLE " . $wpdb->prefix.mi_config::$mi_clubs_championship_db_name);
    }

    public static function freeChampionshipMembersDB() {
        self::freeDB(mi_config::$mi_clubs_championship_members_db_name);
    }

    public static function freeChampionshipSettingsDB() {
        self::freeDB(mi_config::$mi_clubs_championship_settings_db_name);
    }

    public static function freeChampionshipStatusDB() {
        self::freeDB(mi_config::$mi_clubs_championship_status_db_name);
    }

    public static function freeChampionshipStatsDB() {
        self::freeDB(mi_config::$mi_clubs_championship_stats_db_name);
    }

    public static function freeChampionshipResultsDB() {
        self::freeDB(mi_config::$mi_clubs_championship_results_db_name);
    }

    public static function uninstallClubsDB() {
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS " . $wpdb->prefix . mi_config::$mi_clubs_db_name . ";");
    }

    public static function uninstallClubsMemberDB() {
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS " . $wpdb->prefix . mi_config::$mi_clubs_member_db_name . ";");
    }

    public static function uninstallClubsCategoryDB() {
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS " . $wpdb->prefix . mi_config::$mi_clubs_category_db_name . ";");
    }

    public static function uninstallChampionshipDB() {
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS " . $wpdb->prefix . mi_config::$mi_clubs_championship_db_name . ";");
    }

    public static function uninstallChampionshipMembersDB() {
        self::uninstallDB(mi_config::$mi_clubs_championship_members_db_name);
    }

    public static function uninstallChampionshipSettingsDB() {
        self::uninstallDB(mi_config::$mi_clubs_championship_settings_db_name);
    }

    public static function uninstallChampionshipStatusDB() {
        self::uninstallDB(mi_config::$mi_clubs_championship_status_db_name);
    }

    public static function uninstallChampionshipStatsDB() {
        self::uninstallDB(mi_config::$mi_clubs_championship_stats_db_name);
    }

    public static function uninstallChampionshipResultsDB() {
        self::uninstallDB(mi_config::$mi_clubs_championship_results_db_name);
    }

    public static function installAll() {
        self::installClubsCategoryDB();
        self::installClubsDB();
        self::installClubsMemberDB();
        self::installChampionshipMembersDB();
        self::installChampionshipDB();
        self::installChampionshipSettingsDB();
        self::installChampionshipStatusDB();
        self::installChampionshipStatsDB();
        self::installChampionshipResultsDB();
    }

    public static function uninstallAll() {
        self::uninstallClubsMemberDB();
        self::uninstallClubsDB();
        self::uninstallClubsCategoryDB();
        self::uninstallChampionshipMembersDB();
        self::uninstallChampionshipDB();
        self::uninstallChampionshipSettingsDB();
        self::uninstallChampionshipStatusDB();
        self::uninstallChampionshipStatsDB();
        self::uninstallChampionshipResultsDB();
    }

    public static function freeAll() {
        self::freeClubsMemberDB();
        self::freeClubsDB();
        self::freeClubsCategoryDB();
        self::freeChampionshipMembersDB();
        self::freeChampionshipDB();
        self::freeChampionshipSettingsDB();
        self::freeChampionshipStatusDB();
        self::freeChampionshipStatsDB();
        self::freeChampionshipResultsDB();
    }

    private static function freeDB(string $name) {
        global $wpdb;
        $wpdb->query("TRUNCATE TABLE " . $wpdb->prefix.$name);
    }

    private static function uninstallDB(string $name) {
        global $wpdb;
        $wpdb->query("DROP TABLE IF EXISTS " . $wpdb->prefix . $name . ";");
    }
}
