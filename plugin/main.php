<?php
/*
 * Plugin name: Modélisme Interdépartemental de l’Occitanie
 * Description: The description :3
 * Author: Unknown
 * Version: 1.0.0
 */

/// Config
require_once plugin_dir_path(__FILE__) . '/mi_config.php';
/// Utils
require_once plugin_dir_path(__FILE__) . '/utils/mi_utils.php';
require_once plugin_dir_path(__FILE__) . '/utils/mi_array_utils.php';
require_once plugin_dir_path(__FILE__) . '/utils/mi_credentials_protection.php';
/// Display
require_once plugin_dir_path(__FILE__) . '/modules/club/club_display.php';
require_once plugin_dir_path(__FILE__) . '/modules/member/club_member_display.php';
require_once plugin_dir_path(__FILE__) . '/modules/category/club_category_display.php';
require_once plugin_dir_path(__FILE__) . '/modules/championship/championship_display.php';
require_once plugin_dir_path(__FILE__) . '/modules/stats/stats_display.php';
/// Manager
require_once plugin_dir_path(__FILE__) . '/modules/club/club_manager.php';
require_once plugin_dir_path(__FILE__) . '/modules/member/club_member_manager.php';
require_once plugin_dir_path(__FILE__) . '/modules/category/club_category_manager.php';
require_once plugin_dir_path(__FILE__) . '/modules/championship/championship_manager.php';
require_once plugin_dir_path(__FILE__) . '/modules/championship/championship_members_manager.php';
require_once plugin_dir_path(__FILE__) . '/modules/championship/championship_settings_manager.php';
require_once plugin_dir_path(__FILE__) . '/modules/championship/championship_status_manager.php';
require_once plugin_dir_path(__FILE__) . '/modules/championship/championship_stats_manager.php';
require_once plugin_dir_path(__FILE__) . '/modules/championship/championship_results_manager.php';
/// Objects
require_once plugin_dir_path(__FILE__) . '/modules/championship/championship_settings.php';
require_once plugin_dir_path(__FILE__) . '/modules/championship/championship_club_stat.php';
require_once plugin_dir_path(__FILE__) . '/modules/championship/championship_club_member_stat.php';
/// Widgets
require_once plugin_dir_path(__FILE__) . '/modules/widget/mi_club_widget.php';
require_once plugin_dir_path(__FILE__) . '/modules/widget/mi_member_widget.php';
require_once plugin_dir_path(__FILE__) . '/modules/widget/mi_championship_club_widget.php';
require_once plugin_dir_path(__FILE__) . '/modules/widget/mi_championship_member_widget.php';
/// Main
require_once plugin_dir_path(__FILE__) . '/mi_database.php';
require_once plugin_dir_path(__FILE__) . '/club_admin.php';

new club_admin(__FILE__);
