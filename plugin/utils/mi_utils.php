<?php
class mi_utils {
    public static function adminRedirection(string $page, array $args = []): void {
        $name = self::getRandomName('redirection');
        ///Form
        echo '<form method="get" id="' . $name . '"><input type="hidden" name="page" value="' . $page . '">';
        if(!empty($args)) {
            foreach($args as $key => $value) {
                echo '<input type="hidden" name="' . $key . '" value="' . $value . '">';
            }
        }
        echo '</form>';
        ///JS
        echo '<script>'.
            'document.getElementById("' . $name . '").submit()'.
        '</script>';
    }

    public static function getRandomName(string $prefix = '') {
        return $prefix . '_' . random_int(99999, 9999999);
    }
}