<?php
class mi_array_utils {
    public static function arrayHasRequirements(array $a, array $b, bool $onlyhas = false): bool {
        $output = false;
        if(count($a) > 0 && count($b) > 0) {
            $output = true;
            foreach($b as $key) {
                if(!self::isDefined($a, $key)) {
                    $output = false;
                    break;
                }
            }
            if($output && $onlyhas) {
                foreach($a as $key => $value) {
                    if(self::arrayContain($b, $key)) {
                        $output = false;
                        break;
                    }
                }
            }
        }
        return $output;
    }

    public static function isDefined(array $a, $b):bool {
        $output = false;
        foreach($a as $key => $value) {
            if($key === $b) {
                $output = true;
                break;
            }
        }
        return $output;
    }

    public static function arrayContain(array $a, $b, bool $assoc = false, bool $is_set = false): bool {
        $output = false;
        if(!$assoc) {
            foreach($a as $key) {
                if($key == $b) {
                    $output = true;
                    break;
                }
            }
        }
        else {
            foreach($a as $key => $value) {
                if($key == $b && !($is_set && $value == null)) {
                    $output = true;
                }
            }
        }
        return $output;
    }

    public static function arrayExtract(array $a, array $b): array {
        $output = [];
        foreach($a as $key => $value) {
            if(self::arrayContain($b, $key)) {
                $output[$key] = $value;
            }
        }
        return $output;
    }

    public static function containKeyPair(array $a, string $b, string $c = "page"): bool {
        $output = false;
        if(!empty($a)) {
            foreach($a as $key => $value) {
                if($key === $c && $value === $b) {
                    $output = true;
                    break;
                }
            }
        }
        return $output;
    }

    public static function extractIntAssocData(array $a, string $b): int {
        $output = 0;
        if(self::arrayContain($a, $b, true, true)) {
            if(is_numeric($a[$b])) {
                $output = (int)$a[$b];
            }
        }
        return $output;
    }

    public static function shiftArray(array &$a, $element): void {
        $output = [];
        $temp = self::copyArray($a);
        array_push($output, $element);
        foreach($temp as $value) {
            array_push($output, $value);
        }
        $a = $output;
    }

    public static function copyArray(array $copy): array {
        $output = [];
        foreach($copy as $value) {
            array_push($output, $value);
        }
        return $output;
    }

    public static function arrayIsEqual(array $a, array $b, bool $assoc = false) {
        $output = false;
        if(!empty($a) && !empty($b) && count($a) == count($b)) {
            $output = true;
            if($assoc) {
                foreach($a as $key => $value) {
                    if(!self::arrayContain($b, $key, true, true) || $value != $b[$key]) {
                        $output = false;
                        break;
                    }
                }
            }
            else {
                for($i = 0; $i < count($a); $i++) {
                    if($a[$i] != $b[$i]) {
                        $output = false;
                        break;
                    }
                }
            }
        }
        return $output;
    }

    public static function arrayContainObjectField(array $a, string $field_name, $field_value): bool {
        $output = false;
        if(!empty($a)) {
            foreach($a as $item) {
                if($item->$field_name && $item->$field_name == $field_value) {
                    $output = true;
                    break;
                }
            }
        }
        return $output;
    }
}