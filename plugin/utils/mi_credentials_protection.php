<?php
class mi_credentials_protection {
    public static function email(string $arg): bool {
        return filter_var($arg, FILTER_VALIDATE_EMAIL);
    }

    public static function codepostal(string $arg): bool {
        if(strlen($arg) == 5) { return self::isNumberString($arg); }
        return false;
    }

    public static function tel(string $arg): bool {
        if(strlen($arg) == 10 && $arg[0] == 0) { return self::isNumberString($arg); }
        return false;
    }

    public static function cp(string $arg): bool {
        if(strlen($arg) == 5) { return self::isNumberString($arg); }
        return false;
    }

    public static function adh(string $arg): bool {
        return self::isNumberString($arg);
    }

    private static function isNumberString(string $arg): bool {
        $output = false;
        if(strlen($arg) > 0) {
            $output = true;
            for($i = 0; $i < strlen($arg); $i++) {
                if(!self::isNumberChar($arg[$i])) {
                    $output = false;
                    break;
                }
            }
        }
        return $output;
    }

    private static function isNumberChar(string $char): bool {
        $output = false;
        if(strlen($char) == 1) {
            $alphabet = "0123456789";
            for($i = 0; $i < strlen($alphabet); $i++) {
                if($alphabet[$i] == $char) {
                    $output = true;
                    break;
                }
            }
        }
        return $output;
    }
}
