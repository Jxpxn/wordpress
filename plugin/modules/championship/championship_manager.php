<?php
class championship_manager {
    public static function registerChampionship(array $championship): bool {
        global $wpdb;
        if(mi_array_utils::arrayHasRequirements($championship, mi_config::$club_championship_requirements)) {
            $data = mi_array_utils::arrayExtract($championship, mi_config::$club_championship_requirements);
            if(!self::getChampionshipByName($data['nom'])) {
                $wpdb->insert($wpdb->prefix.mi_config::$mi_clubs_championship_db_name, $data);
                return true;
            }
        }
        return false;
    }

    public static function getChampionshipByName(string $name) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_db_name . " WHERE nom='" . $name . "';");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function getChampionshipById(int $id) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_db_name . " WHERE id=" . (string)$id . ";");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function deleteChampionshipById(int $id): void {
        global $wpdb;
        $wpdb->delete($wpdb->prefix . mi_config::$mi_clubs_championship_db_name, array("id" => $id));
    }

    public static function getChampionshipWithName(): array {
        global $wpdb;
        $output = [];
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_db_name . ";");
        foreach($result as $line) {
            $output[(string)$line->id] = $line->nom;
        }
        return $output;
    }

    public static function getChampionships() {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_db_name . ";");
    }

    public static function updateChampionshipById(int $id, array $championship): bool {
        global $wpdb;
        if(mi_array_utils::arrayHasRequirements($championship, mi_config::$club_championship_requirements)) {
            $data = mi_array_utils::arrayExtract($championship, mi_config::$club_championship_requirements);
            if($data) {
                $wpdb->update($wpdb->prefix.mi_config::$mi_clubs_championship_db_name, $data, ['id' => $id]);
                return true;
            }
        }
        return false;
    }

    public static function updateChampionshipCategoryById(int $id, int $category_id) {
        global $wpdb;
        $wpdb->update($wpdb->prefix.mi_config::$mi_clubs_championship_db_name, ['category' => $category_id], ['id' => $id]);
    }

    public static function getChampionshipsByCategory(int $id) {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_db_name . " WHERE category='" . (string)$id . "';");
    }

    public static function getLastRegisteredChampionship() {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_db_name . " ORDER BY `id` DESC LIMIT 1;");
        if($result && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function clearChampionshipDetails(int $championship_id): void {
        championship_stats_manager::deleteChampionshipStatsByChampionshipId($championship_id);
        championship_status_manager::deleteChampionshipStatusByChampionshipId($championship_id);
        championship_results_manager::deleteChampionshipResultsByChampionshipId($championship_id);
    }
}