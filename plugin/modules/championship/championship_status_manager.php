<?php
class championship_status_manager {
    public static function registerChampionshipStatus(array $championship): bool {
        global $wpdb;

        if(mi_array_utils::arrayHasRequirements($championship, mi_config::$club_championship_status_requirements)) {
            $data = mi_array_utils::arrayExtract($championship, mi_config::$club_championship_status_requirements);
            if(championship_manager::getChampionshipById((int)$data['championship_id'])) {
                $wpdb->insert($wpdb->prefix.mi_config::$mi_clubs_championship_status_db_name, $data);
                return true;
            }
        }
        return false;
    }

    public static function getChampionshipStatusByChampionshipId(int $id) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_status_db_name . " WHERE championship_id=" . (string)$id . ";");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function getChampionshipStatusById(int $id) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_status_db_name . " WHERE id=" . (string)$id . ";");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function deleteChampionshipStatusById(int $id): void {
        global $wpdb;
        $wpdb->delete($wpdb->prefix . mi_config::$mi_clubs_championship_status_db_name, array("id" => $id));
    }

    public static function deleteChampionshipStatusByChampionshipId(int $id): void {
        global $wpdb;
        $wpdb->delete($wpdb->prefix . mi_config::$mi_clubs_championship_status_db_name, array("championship_id" => $id));
    }

    public static function getChampionshipStatus() {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_status_db_name . ";");
    }

    public static function updateChampionshipStatusById(int $id, array $championship): bool {
        global $wpdb;
        if(mi_array_utils::arrayHasRequirements($championship, mi_config::$club_championship_status_requirements)) {
            $data = mi_array_utils::arrayExtract($championship, mi_config::$club_championship_status_requirements);
            if($data) {
                $wpdb->update($wpdb->prefix.mi_config::$mi_clubs_championship_status_db_name, $data, ['id' => $id]);
                return true;
            }
        }
        return false;
    }

    public static function updateChampionshipStatusByChampionshipId(int $id, array $championship): bool {
        global $wpdb;
        if(mi_array_utils::arrayHasRequirements($championship, mi_config::$club_championship_status_requirements)) {
            $data = mi_array_utils::arrayExtract($championship, mi_config::$club_championship_status_requirements);
            if($data) {
                $wpdb->update($wpdb->prefix.mi_config::$mi_clubs_championship_status_db_name, $data, ['championship_id' => $id]);
                return true;
            }
        }
        return false;
    }
}