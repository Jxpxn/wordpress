<?php
class championship_settings_manager {
    public static function registerChampionshipSettings(array $championship): bool {
        global $wpdb;

        if(mi_array_utils::arrayHasRequirements($championship, mi_config::$club_championship_settings_requirements)) {
            $data = mi_array_utils::arrayExtract($championship, mi_config::$club_championship_settings_requirements);
            if(championship_manager::getChampionshipById((int)$data['championship_id'])) {
                $wpdb->insert($wpdb->prefix.mi_config::$mi_clubs_championship_settings_db_name, $data);
                return true;
            }
        }
        return false;
    }

    public static function getChampionshipSettingsByChampionshipId(int $id) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_settings_db_name . " WHERE championship_id=" . (string)$id . ";");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function getChampionshipSettingsById(int $id) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_settings_db_name . " WHERE id=" . (string)$id . ";");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function deleteChampionshipSettingsById(int $id): void {
        global $wpdb;
        $wpdb->delete($wpdb->prefix . mi_config::$mi_clubs_championship_settings_db_name, array("id" => $id));
    }

    public static function getChampionshipSettings() {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_settings_db_name . ";");
    }

    public static function updateChampionshipSettingsById(int $id, array $championship): bool {
        global $wpdb;
        if(mi_array_utils::arrayHasRequirements($championship, mi_config::$club_championship_settings_requirements)) {
            $data = mi_array_utils::arrayExtract($championship, mi_config::$club_championship_settings_requirements);
            if($data) {
                $wpdb->update($wpdb->prefix.mi_config::$mi_clubs_championship_settings_db_name, $data, ['id' => $id]);
                return true;
            }
        }
        return false;
    }

    public static function updateChampionshipSettingsByChampionshipId(int $id, array $championship): bool {
        global $wpdb;
        if(mi_array_utils::arrayHasRequirements($championship, mi_config::$club_championship_settings_requirements)) {
            $data = mi_array_utils::arrayExtract($championship, mi_config::$club_championship_settings_requirements);
            if($data) {
                $wpdb->update($wpdb->prefix.mi_config::$mi_clubs_championship_settings_db_name, $data, ['championship_id' => $id]);
                return true;
            }
        }
        return false;
    }
}