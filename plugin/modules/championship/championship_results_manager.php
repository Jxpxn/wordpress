<?php
class championship_results_manager {
    public static function registerChampionshipResult(array $championship): bool {
        global $wpdb;

        if(mi_array_utils::arrayHasRequirements($championship, mi_config::$club_championship_results_requirements)) {
            $data = mi_array_utils::arrayExtract($championship, mi_config::$club_championship_results_requirements);
            if(championship_manager::getChampionshipById((int)$data['championship_id'])) {
                $wpdb->insert($wpdb->prefix.mi_config::$mi_clubs_championship_results_db_name, $data);
                return true;
            }
        }
        return false;
    }

    public static function getChampionshipResultById(int $id) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_results_db_name . " WHERE id=" . (string)$id . ";");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function getChampionshipResultsByChampionshipId(int $id) {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_results_db_name . " WHERE championship_id=" . (string)$id . ";");
    }

    public static function getChampionshipResultsByChampionshipIdAndConf(int $id, int $conf) {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_results_db_name . " WHERE championship_id=" . (string)$id . " AND conf=" . (string)$conf . ";");
    }

    public static function getChampionshipStatsByClubMemberId(int $id) {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_results_db_name . " WHERE member_id=" . (string)$id . ";");
    }

    public static function getChampionshipStatsByChampionshipIdAndClubMemberId(int $championship_id, int $member_id) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_results_db_name . " WHERE member_id=" . (string)$member_id . " AND championship_id=" . (string)$championship_id . ";");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function deleteChampionshipStatsById(int $id): void {
        global $wpdb;
        $wpdb->delete($wpdb->prefix . mi_config::$mi_clubs_championship_results_db_name, array("id" => $id));
    }

    public static function deleteChampionshipResultsByChampionshipId(int $id): void {
        global $wpdb;
        $wpdb->delete($wpdb->prefix . mi_config::$mi_clubs_championship_results_db_name, array("championship_id" => $id));
    }

    public static function deleteChampionshipResultsByChampionshipIdAndStateId(int $championship_id, int $conf, int $manche): void {
        global $wpdb;
        $wpdb->delete($wpdb->prefix . mi_config::$mi_clubs_championship_results_db_name, array("championship_id" => $championship_id, "conf" => $conf, "manche" => $manche));
    }

    public static function getChampionshipResults() {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_results_db_name . ";");
    }

    public static function updateChampionshipResultsById(int $id, array $championship): bool {
        global $wpdb;
        if(mi_array_utils::arrayHasRequirements($championship, mi_config::$club_championship_results_requirements)) {
            $data = mi_array_utils::arrayExtract($championship, mi_config::$club_championship_results_requirements);
            if($data) {
                $wpdb->update($wpdb->prefix.mi_config::$mi_clubs_championship_results_db_name, $data, ['id' => $id]);
                return true;
            }
        }
        return false;
    }

    public static function updateChampionshipResultsByChampionshipId(int $id, array $championship): bool {
        global $wpdb;
        if(mi_array_utils::arrayHasRequirements($championship, mi_config::$club_championship_results_requirements)) {
            $data = mi_array_utils::arrayExtract($championship, mi_config::$club_championship_results_requirements);
            if($data) {
                $wpdb->update($wpdb->prefix.mi_config::$mi_clubs_championship_results_db_name, $data, ['championship_id' => $id]);
                return true;
            }
        }
        return false;
    }

    public static function updateChampionshipResultsByChampionshipIdAndState(int $championship_id, int $conf, int $manche, array $championship): bool {
        global $wpdb;
        if(mi_array_utils::arrayHasRequirements($championship, mi_config::$club_championship_results_requirements)) {
            $data = mi_array_utils::arrayExtract($championship, mi_config::$club_championship_results_requirements);
            if($data) {
                $wpdb->update($wpdb->prefix.mi_config::$mi_clubs_championship_results_db_name, $data, ['championship_id' => $championship_id, "conf" => $conf, "manche" => $manche]);
                return true;
            }
        }
        return false;
    }
}