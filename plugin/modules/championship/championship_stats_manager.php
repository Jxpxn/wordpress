<?php
class championship_stats_manager {
    public static function registerChampionshipStats(array $championship): bool {
        global $wpdb;

        if(mi_array_utils::arrayHasRequirements($championship, mi_config::$club_championship_stats_requirements)) {
            $data = mi_array_utils::arrayExtract($championship, mi_config::$club_championship_stats_requirements);
            if(championship_manager::getChampionshipById((int)$data['championship_id']) && club_member_manager::getClubMemberById((int)$data['member_id'])) {
                $wpdb->insert($wpdb->prefix.mi_config::$mi_clubs_championship_stats_db_name, $data);
                return true;
            }
        }
        return false;
    }

    public static function getChampionshipStatsById(int $id) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_stats_db_name . " WHERE id=" . (string)$id . ";");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function getChampionshipStatsByChampionshipId(int $id) {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_stats_db_name . " WHERE championship_id=" . (string)$id . ";");
    }

    public static function getChampionshipStatsByClubMemberId(int $id) {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_stats_db_name . " WHERE member_id=" . (string)$id . ";");
    }

    public static function getChampionshipStatsByChampionshipIdAndClubMemberId(int $championship_id, int $member_id) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_stats_db_name . " WHERE member_id=" . (string)$member_id . " AND championship_id=" . (string)$championship_id . ";");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function deleteChampionshipStatsById(int $id): void {
        global $wpdb;
        $wpdb->delete($wpdb->prefix . mi_config::$mi_clubs_championship_stats_db_name, array("id" => $id));
    }

    public static function deleteChampionshipStatsByChampionshipId(int $id): void {
        global $wpdb;
        $wpdb->delete($wpdb->prefix . mi_config::$mi_clubs_championship_stats_db_name, array("championship_id" => $id));
    }

    public static function deleteChampionshipStatsByClubMemberId(int $id): void {
        global $wpdb;
        $wpdb->delete($wpdb->prefix . mi_config::$mi_clubs_championship_stats_db_name, array("member_id" => $id));
    }

    public static function deleteChampionshipStatsByChampionshipIdAndClubMemberId(int $championship_id, int $member_id): void {
        global $wpdb;
        $wpdb->delete($wpdb->prefix . mi_config::$mi_clubs_championship_stats_db_name, array("championship_id" => $championship_id, "member_id" => $member_id));
    }

    public static function getChampionshipStats() {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_stats_db_name . ";");
    }

    public static function updateChampionshipStatsById(int $id, array $championship): bool {
        global $wpdb;
        if(mi_array_utils::arrayHasRequirements($championship, mi_config::$club_championship_stats_requirements)) {
            $data = mi_array_utils::arrayExtract($championship, mi_config::$club_championship_stats_requirements);
            if($data) {
                $wpdb->update($wpdb->prefix.mi_config::$mi_clubs_championship_stats_db_name, $data, ['id' => $id]);
                return true;
            }
        }
        return false;
    }

    public static function updateChampionshipStatusByChampionshipId(int $id, array $championship): bool {
        global $wpdb;
        if(mi_array_utils::arrayHasRequirements($championship, mi_config::$club_championship_stats_requirements)) {
            $data = mi_array_utils::arrayExtract($championship, mi_config::$club_championship_stats_requirements);
            if($data) {
                $wpdb->update($wpdb->prefix.mi_config::$mi_clubs_championship_stats_db_name, $data, ['championship_id' => $id]);
                return true;
            }
        }
        return false;
    }

    public static function updateChampionshipStatusByChampionshipIdAndClubMemberId(int $championship_id, int $member_id, array $championship): bool {
        global $wpdb;
        if(mi_array_utils::arrayHasRequirements($championship, mi_config::$club_championship_stats_requirements)) {
            $data = mi_array_utils::arrayExtract($championship, mi_config::$club_championship_stats_requirements);
            if($data) {
                $wpdb->update($wpdb->prefix.mi_config::$mi_clubs_championship_stats_db_name, $data, ['championship_id' => $championship_id, "member_id" => $member_id]);
                return true;
            }
        }
        return false;
    }
}