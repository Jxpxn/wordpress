<?php
class championship_display {
    public static function displayTable() {
        echo "<h1>".get_admin_page_title()."</h1>";
        echo "<form method='post' id='club_db_form'>";
        echo '<input type="hidden" name="type" value="delete">';
        echo '<table class="widefat fixed" cellspacing="0">';
        echo '<tr><th class="manage-column column-columnname " scope="col">Select</th>' .
            '<th class="manage-column column-columnname " scope="col">Id</th>' .
            '<th class="manage-column column-columnname " scope="col">Nom</th>' .
            '<th class="manage-column column-columnname " scope="col">Catégorie</th>' .
            '<th class="manage-column column-columnname " scope="col">Membres</th></tr>';
        foreach (championship_manager::getChampionships() as $line) {
            $category = club_category_manager::getClubCategoryById($line->category);
            echo '<tr style="border:1px solid #000">';
            echo '<td><input class="widefat" type="checkbox" name="selected_championship_' . $line->id . '" value="y" /></td>';
            echo '<td>' . $line->id . '</td>';
            echo '<td>' . $line->nom . '</td>';
            if($category) {
                echo '<td>' . $category->nom . '</td>';
            }
            else if($line->category == 0) {
                echo '<td>Aucune catégorie</td>';
            }
            else {
                echo '<td>Catégorie inconnu</td>';
            }
            echo '<td><a title="Voir profil" href="?page=club_championship_db&members=' . $line->id . '">Voir</a></td>';
            echo '<td><a title="Voir championnat" href="?page=club_championship_db&status=' . $line->id . '">👁️</a></td>';
            echo '<td><a title="Editer championnat" href="?page=club_championship_db&edit=' . $line->id . '">✏️</a></td>';
            echo '</tr>';
        }
        echo '<tr><th class="manage-column column-columnname " scope="col"><label onclick="document.getElementById(`club_db_form`).submit();">Supprimer</label></th></tr>';
        echo '</table>';
        echo '</form>';
    }

    public static function displayAdder() {
        if(mi_array_utils::containKeyPair($_POST, "add", "type")) {
            if(championship_manager::registerChampionship($_POST)) {
                $championship_ = championship_manager::getLastRegisteredChampionship();
                championship_settings_manager::registerChampionshipSettings(['championship_id' => $championship_->id, "settings" => serialize(new championship_settings())]);
                mi_utils::adminRedirection('club_championship_db');
                return;
            }
            else {
                echo "<h1>Une erreur est survenu lors de l'inscription du championnat :/</h1>";
            }
        }

        echo "<h1>".get_admin_page_title()."</h1>";

        echo '<form action="" method="post" style="padding-right: 15px;">';
        echo '<input type="hidden" name="type" value="add">';
        echo '<p>';
        echo '<label for="nom">Nom :</label>';
        echo '<input class="widefat" id="nom" name="nom" type="text" value="" />';
        echo '</p>';
        echo '<p>';
        echo '<label for="category">Catégorie : </label>';
        echo '<select id="category" name="category">';
        echo '<option value="0">Aucun</option>';
        foreach(club_category_manager::getClubsCategory() as $category) {
            echo '<option value="' . $category->id . '">' . $category->nom . '</option>';
        }
        echo '</select>';
        echo '</p>';
        echo '<p><input type="submit" value="Enregister"></p>';
        echo '</form>';
    }

    public static function displayMemberAdder(string $championship_id) {
        if(mi_array_utils::containKeyPair($_POST, "add", "type")) {
            if(championship_members_manager::registerChampionshipMember($_POST)) {
                championship_manager::clearChampionshipDetails($championship_id);
                mi_utils::adminRedirection('club_championship_db', ['members' => $_POST['championship_id']]);
                return;
            }
            else if(mi_array_utils::arrayContain($_POST, "club_id", true, true) && $_POST['club_id'] == 0) {
                mi_utils::adminRedirection('club_championship_db', ['members' => $_POST['championship_id']]);
                return;
            }
            else {
                echo "<h4>Une erreur est survenu lors de l'inscription, ce club est déja enregistrer dans ce championnat :/</h4>";
            }
        }

        if(is_numeric($championship_id)) {
            $championship_id_int = (int)$championship_id;
            $championship = championship_manager::getChampionshipById($championship_id_int);
            if($championship) {
                $current_club = championship_members_manager::getChampionshipMembersByChampionshipId($championship_id_int);
                $flag_ = championship_status_manager::getChampionshipStatusByChampionshipId($championship_id_int);
                $available_clubs = [];
                foreach(club_manager::getClubsByCategory($championship->category) as $club) {
                    if($club->participant == 1) {
                        $flag = false;
                        if($current_club) {
                            foreach($current_club as $club_) {
                                if($club_->club_id == $club->id) {
                                    $flag = true;
                                    break;
                                }
                            }
                        }
                        if(!$flag) {
                            array_push($available_clubs, $club);
                        }
                    }
                }
                echo "<h1>Ajouter un membre d'un championnat: </h1>";
                echo "<h3>Championnat: " . $championship->nom . "</h3>";

                echo '<form action="" method="post" style="padding-right: 15px;">';
                echo '<input type="hidden" name="type" value="add">';
                echo '<input type="hidden" name="championship_id" value="' . $championship_id . '">';
                echo '<p>';
                echo '<label for="club_id">Club : </label>';
                echo '<select id="club_id" name="club_id">';
                if(count($available_clubs) > 0) {
                    foreach($available_clubs as $club) {
                        echo '<option value="' . $club->id . '">' . $club->nom . '</option>';
                    }
                }
                else {
                    echo '<option value="0">Aucun club disponible</option>';
                }
                echo '</select>';
                echo '</p>';
                if($flag_) {
                    echo '<p style="color: red;">En mettant à jour les paramètres du championnat, le championnat sera réinitialiser ! <br>';
                }
                else {
                    echo '<p>';
                }
                echo '<input type="submit" value="Enregister"></p>';
                echo '</form>';
            }
        }
        else {
            self::displayTable();
        }
    }

    public static function displayDelete(array $championships) {
        $todelete = [];
        foreach($championships as $key => $value) {
            if($value === 'y') {
                $parsed = explode('selected_championship_', $key);
                $parsed = $parsed[count($parsed)-1];
                if(is_numeric($parsed)) {
                    array_push($todelete, $parsed);
                }
            }
        }
        foreach($todelete as $championship) {
            championship_manager::clearChampionshipDetails((int)$championship_id);
            championship_members_manager::deleteChampionshipMembersByChampionshipId((int)$championship);
            championship_manager::deleteChampionshipById((int)$championship);
        }
        self::displayTable();
    }

    public static function displayConfEdit(string $category_id, array $args) {
        if(is_numeric($category_id)) {
            $category_id_int = (int)$category_id;
            $category = championship_manager::getChampionshipById($category_id_int);
            if(!$category || !championship_manager::updateChampionshipById($category_id_int, $args)) {
                echo "Erreur lors de l'enregistrement de la catégorie :/";
                self::displayEditor($category_id);
                return;
            }
            championship_manager::clearChampionshipDetails((int)$category_id);
        }
        mi_utils::adminRedirection('club_championship_db');
    }

    public static function displayEditor(string $championship_id) {
        if(is_numeric($championship_id)) {
            $championship_id_int = (int)$championship_id;
            $championship = championship_manager::getChampionshipById($championship_id_int);
            if($championship) {
                $flag_ = championship_status_manager::getChampionshipStatusByChampionshipId($championship_id_int);
                echo '<h1>Championnat n°' . $championship->id . ' '. self::getToolBar($championship_id_int) . '</h1>';
                echo '<form action="" method="post" style="padding-right: 15px;">';
                echo '<input type="hidden" name="conf_edit" value="' . $championship->id . '">';
                echo '<p>';
                echo '<label for="nom">Nom :</label>';
                echo '<input class="widefat" id="nom" name="nom" type="text" value="' . $championship->nom . '" />';
                echo '</p>';
                echo '<p>';
                echo '<label for="category">Catégorie : </label>';
                echo '<select id="category" name="category">';
                echo '<option value="0">Aucun</option>';
                foreach(club_category_manager::getClubsCategory() as $category) {
                    echo '<option ';
                    if($category->id == $championship->category) {
                        echo 'selected="selected"';
                    }
                    echo ' value="' . $category->id;
                    echo '">' . $category->nom . '</option>';
                }
                echo '</select>';
                echo '</p>';
                if($flag_) {
                    echo '<p style="color: red;">En mettant à jour les paramètres du championnat, le championnat sera réinitialiser ! <br>';
                }
                else {
                    echo '<p>';
                }
                echo '<input type="submit" value="Enregister"></p>';
                echo '</form>';
            }
        }
        else {
            self::displayTable();
        }
    }

    public static function displayMembers(string $category_id) {
        if(is_numeric($category_id)) {
            $category_id_num = (int)$category_id;
            $category = championship_manager::getChampionshipById($category_id_num);
            if($category) {
                $members = championship_members_manager::getChampionshipMembersByChampionshipId($category_id_num);
                $members_count = $members ? count($members) : 0;
                echo '<h1>Championnat : ' .  $category->nom .' <td>' . self::getToolBar($category_id_num) . '</td></h1>';
                echo '<h2>Clubs membres (' . (string)$members_count .')</h2>';
                echo '<a href="?page=club_championship_db&championship_member_adder=' . $category_id . '">Enregistrer un club</a>';
                if($members_count > 0) {
                    echo '<form method="post" id="club_db_form">';
                    echo '<table class="widefat fixed" cellspacing="0">';
                    echo '<input type="hidden" name="type" value="kick">';
                    echo '<input type="hidden" name="championship" value="' . $category_id . '">';
                    echo '<tr><th class="manage-column column-columnname " scope="col">Select</th>' .
                        '<th class="manage-column column-columnname " scope="col">Id</th>' .
                        '<th class="manage-column column-columnname " scope="col">Nom</th>' .
                        '<th class="manage-column column-columnname " scope="col">Profil</th></tr>';
                    foreach ($members as $line) {
                        $selected_club = club_manager::getClubById($line->club_id);
                        $selected_club_id = $selected_club ? $selected_club->id : '0';
                        echo '<tr style="border:1px solid #000">';
                        echo '<td><input class="widefat" type="checkbox" name="selected_championship_member_' . $line->id . '" value="y" /></td>';
                        echo '<td>' . $line->id . '</td>';
                        if($selected_club) {
                            echo '<td>' . ucfirst($selected_club->nom) . '</td>';
                        }
                        else {
                            echo '<td>Club inconnu</td>';
                        }
                        echo '<td><a title="Voir profil" href="?page=club_db&profil=' . $selected_club_id . '">Voir</a></td>';
                        echo '<td><a title="Editer club" href="?page=club_db&edit=' . $selected_club_id . '">✏️</a></td>';
                        echo '</tr>';
                    }
                    echo '<tr><th class="manage-column column-columnname " scope="col"><label ';
                    $flag = championship_status_manager::getChampionshipStatusByChampionshipId($category_id_num);
                    if($flag) {
                        echo 'title="En mettant à jour les membres du championnat, le championnat sera réinitialiser !"';
                    }
                    echo 'onclick="document.getElementById(`club_db_form`).submit();">' . ($flag ? '⚠️' : '') . 'Ejecter</label></th></tr>';
                    echo '</table>';
                    echo '</form>';
                }
                else {
                    echo '<h4>Ce championnat n' . "'" . 'a aucun participant :/</h4>';
                }
            }
        }
    }

    public static function displayKick(array $clubs) {
        $todelete = [];
        $club_id = mi_array_utils::arrayContain($clubs, 'championship', true, true) ? $clubs['championship'] : '0';
        foreach($clubs as $key => $value) {
            if($value === 'y') {
                $parsed = explode('selected_championship_member_', $key);
                $parsed = $parsed[count($parsed)-1];
                if(is_numeric($parsed)) {
                    if($club_id == 0) {
                        $member = championship_members_manager::getChampionshipMemberById((int)$parsed);
                        if($member) {
                            $club_id = $member->club;
                        }
                    }
                    array_push($todelete, $parsed);
                }
            }
        }
        if($todelete && !empty($todelete)) {
            championship_manager::clearChampionshipDetails((int)$club_id);
        }
        foreach($todelete as $club) {
            championship_members_manager::deleteChampionshipMemberById((int)$club);
        }
        self::displayMembers($club_id);
    }

    public static function displaySettingsEditor(string $championship_id) {
        if(is_numeric($championship_id)) {
            $championship_id_int = (int)$championship_id;

            if(mi_array_utils::containKeyPair($_POST, "edit", "type")) {
                $settings = championship_settings_manager::getChampionshipSettingsByChampionshipId($championship_id_int);
                $settings = $settings && !empty($settings) ? unserialize($settings->settings) : new championship_settings();

                $my_arg = self::convertToSettingsArgs($_POST);
                $my_arg['championship_id'] = (int)$championship_id;
                
                if(championship_settings_manager::updateChampionshipSettingsByChampionshipId((int)$championship_id, $my_arg)) {
                    $new_settings = unserialize($my_arg['settings']);
                    if($new_settings->c != $settings->c || $new_settings->m != $settings->m || !mi_array_utils::arrayIsEqual($settings->pts, $new_settings->pts)) {
                        championship_manager::clearChampionshipDetails($championship_id_int);
                    }
                    mi_utils::adminRedirection('club_championship_db', ['status' => $championship_id]);
                    return;
                }
                else {
                    echo "<h4>Une erreur est survenu lors de l'edition du championnat :/</h4>";
                }
            }
            
            $championship = championship_manager::getChampionshipById($championship_id_int);
            if($championship) {
                // Utils
                $current_settings = championship_settings_manager::getChampionshipSettingsByChampionshipId($championship_id_int);
                $current_settings = $current_settings ? unserialize($current_settings->settings) : new championship_settings();
                $flag_ = championship_status_manager::getChampionshipStatusByChampionshipId($championship_id_int);
                $add_func_name = mi_utils::getRandomName('func');
                $add_class_name = mi_utils::getRandomName('div_score');
                $add_label_name = mi_utils::getRandomName('score_count');
                //Render
                echo '<h1>Championnat n°' . $championship_id . ' : ' . $championship->nom . ' ' . self::getToolBar($championship_id_int) . '</h1>';
                echo '<form action="" method="post" style="padding-right: 15px;">';
                echo '<input type="hidden" name="type" value="edit">';
                echo '<input type="hidden" name="championship" value="' . $championship->id . '">';
                echo '<p>';
                echo '<label for="conf">Nombre de confrontations :</label>';
                echo '<input class="widefat" style="display: block; width: inherit;" id="conf" name="conf" type="number" value="' . $current_settings->c . 
                '" min="' . mi_config::$mi_championship_settings_c_min . '" max="' . mi_config::$mi_championship_settings_c_max . '">';
                echo '</p>';
                echo '<p>';
                echo '<label for="manche">Nombre de manches :</label>';
                echo '<input class="widefat" style="display: block; width: inherit;" id="manche" name="manche" type="number" value="' . $current_settings->m . 
                '" min="' . mi_config::$mi_championship_settings_m_min . '" max="' . mi_config::$mi_championship_settings_m_max . '">';
                echo '</p>';
                echo '<label for="pts_score_main_div">Points :</label>';
                echo '<div id="pts_score_main_div" style="background-color: #FFF; padding: 15px; width: 15%;">';
                echo '<label id="' . $add_label_name . '">Ajouter score ' . count($current_settings->pts) . '/' . mi_config::$mi_championship_settings_max_pts_count . '</label>';
                echo ' <label style="border-radius: 50%; border: 1px solid black; padding: 0px 5px 0px 5px; margin-left: 5px; user-select: none;" onclick="' . $add_func_name .'();">+</label>';
                echo ' <label style="border-radius: 50%; border: 1px solid black; padding: 0px 5px 0px 5px; margin-left: 5px; user-select: none;" onclick="' . $add_func_name .'(true);">-</label>';
                for($i = 0; $i < count($current_settings->pts); $i++) {
                    $current_id_ = $i + 1;
                    echo '<div class="' . $add_class_name . '" style="margin-top: 10px; padding: 5px; border-radius: 5px;">';
                    echo '<label>n°' . $current_id_ . '</label> <br> Points: ';
                    echo '<input type="number" name="score_pts_' . $current_id_ . '" style="display: block; width: inherit; display: inline-block;" value="' . $current_settings->pts[$i] . '" min="0" max="1000">';
                    echo '</div>';
                }
                echo '</div>';
                if($flag_) {
                    echo '<p style="color: red;">En mettant à jour les paramètres du championnat, le championnat sera réinitialiser ! <br>';
                }
                else {
                    echo '<p>';
                }
                echo '<input type="submit" value="Sauvegarder"></p>';
                echo '</form>';
                // Javascript
                echo '<script>';
                echo 'const score_div_instance = document.getElementById("pts_score_main_div");';
                echo 'const score_label_count_instance = document.getElementById("' . $add_label_name . '");';
                echo 'function ' . $add_func_name . '(delete_mode = false) {' .
                    'let current_scores = document.querySelectorAll(".' . $add_class_name . '"); let current_scores_count = current_scores.length;' .
                    'if(delete_mode && current_scores_count > 1) {score_div_instance.removeChild(current_scores[current_scores_count - 1]); current_scores_count--; } else if (!delete_mode && current_scores_count < ' . mi_config::$mi_championship_settings_max_pts_count . ') { score_div_instance.appendChild(getNewScoreElement(current_scores_count + 1)); current_scores_count++; }' .
                    'score_label_count_instance.innerHTML = "Ajouter score " + current_scores_count + "/' . mi_config::$mi_championship_settings_max_pts_count . '" }' .
                    'function getNewScoreElement(id) {' . 
                        'let output_div = document.createElement("div"); output_div.classList.add("' . $add_class_name . '"); output_div.setAttribute("style", "margin-top: 10px; padding: 5px; border-radius: 5px;");'.
                        'let output_label = document.createElement("label"); output_label.innerHTML = "n°" + id;'.
                        'let output_input = document.createElement("input"); output_input.setAttribute("name", "score_pts_" + id);'.
                        'output_input.setAttribute("style", "display: block; width: inherit; display: inline-block;"); output_input.setAttribute("type", "number");'.
                        'output_input.setAttribute("value", "0"); output_input.setAttribute("min", "0"); output_input.setAttribute("max", "1000");'.
                        'output_div.appendChild(output_label); output_div.innerHTML += "<br> Points: "; output_div.appendChild(output_input); return output_div;'.
                        '}';
                echo '</script>';
                return;
            }
        }
        mi_utils::adminRedirection("club_championship_db");
    }

    private static function convertToSettingsArgs(array $arg): array {
        $output = [];
        $conf = mi_array_utils::extractIntAssocData($arg, 'conf');
        $manche = mi_array_utils::extractIntAssocData($arg, 'manche');
        $obj = new championship_settings();
        $obj->c = $conf;
        $obj->m = $manche;
        $new_pts = [];
        for($i = 0; $i < mi_config::$mi_championship_settings_max_pts_count; $i++) {
            $id = 'score_pts_'.($i+1);
            if(mi_array_utils::arrayContain($arg, $id, true, true) && is_numeric($arg[$id])) {
                array_push($new_pts, (int)$arg[$id]);
            }
            else {
                break;
            }
        }
        if(empty($new_pts)) {
            $new_pts = [0];
        }
        $obj->pts = $new_pts;
        $output['settings'] = serialize($obj);
        return $output;
    }

    public static function displayStatus(string $championship_id) {
        if(is_numeric($championship_id)) {
            $championship_id_int = (int)$championship_id;
            $championship = championship_manager::getChampionshipById($championship_id_int);
            if($championship) {
                ///Utils
                $championship_status = championship_status_manager::getChampionshipStatusByChampionshipId($championship_id_int);
                $current_settings = championship_settings_manager::getChampionshipSettingsByChampionshipId($championship_id_int);
                $current_settings = $current_settings ? unserialize($current_settings->settings) : new championship_settings();
                $category = club_category_manager::getClubCategoryById($championship->category);
                $championship_members = self::getClubsInfo($championship_id_int, ($championship_status ? true : false));
                $championship_members_members = $championship_status ? $championship_members[1] : 0;
                $championship_members = $championship_members[0];
                $championship_isfinished = $championship_status ? $championship_status->finish != 0 : false;
                ///Render
                echo '<h1>Championnat n°' . $championship_id . ' : ' . $championship->nom . ' ' . self::getToolBar($championship_id_int) . '</h1>';
                echo '<p>';
                echo 'En cours: ' . ($championship_status && !$championship_isfinished ? '✔️' : '❌') . ($championship_isfinished ? ' (Terminé)' : '') . '<br>';
                echo 'Confrontation actuelle: ' . ($championship_status ? $championship_status->conf : '0') . '/' . $current_settings->c . '<br>';
                echo 'Manche actuelle: ' . ($championship_status ? $championship_status->manche : '0') . '/' . $current_settings->m . '<br>';
                echo 'Catégorie: ';
                if($category) {
                    echo '<a title="Voir membres" href="?page=club_category_db&members=' . (string)$category->id . '">' . $category->nom . '</a>';
                }
                else if($championship->category == 0) {
                    echo 'Aucune catégorie';
                }
                else {
                    echo 'Catégorie inconnu';
                }
                echo '</p>';
                echo '<h4>' . ($championship_members > 0 ? '<a style="text-decoration: none;" href="?page=club_championship_db&d_members=' . $championship_id . '">Liste des clubs participants (' . (string)$championship_members . ')</a>' : 'Aucun club participant');
                echo '<br>' . ($championship_members_members > 0 ? '<a style="text-decoration: none;" href="?page=club_championship_db&d_members_members=' . $championship_id . '">Liste des participants (' . (string)$championship_members_members . ')</a>' : 'Aucun participant')  . '<br>';
                if($championship_status && ((int)$championship_status->conf > 1 || (int)$championship_status->manche > 1 || $championship_status->finish != 0)) { echo '<a style="text-decoration: none;" href="?page=club_championship_db&results=' . $championship_id . '">Voir les résultats</a>'; }
                echo '</h4>';
                echo '<p>';
                echo '<form method="get" style="display: inline-block;">';
                echo '<input type="hidden" name="page" value="club_championship_db">';
                echo '<input type="hidden" name="switch_status" value="' . $championship_id . '">';
                echo '<input type="submit" value="' . ($championship_status ? 'Réinitialiser' : 'Démarrer') . '">';
                echo '</form>';
                if($championship_status && !$championship_isfinished) {
                    echo '<form method="get" style="display: inline-block;">';
                    echo '<input type="hidden" name="page" value="club_championship_db">';
                    echo '<input type="hidden" name="update_result" value="' . $championship_id . '">';
                    echo '<input type="submit" value="Mettre à jour les resultats">';
                    echo '</form>';
                }
                echo '</p>';
                return;
            }
        }
        mi_utils::adminRedirection("club_championship_db");
    }

    private static function getClubsInfo(int $championship_id, bool $only_participant = false) {
        $output = [0, 0];
        $members = championship_members_manager::getChampionshipMembersByChampionshipId($championship_id);
        if($members && !empty($members)) {
            $output[0] = count($members);
            foreach($members as $member) {
                $club_members = club_member_manager::getClubMemberbyClub((int)$member->club_id);
                if($club_members && !empty($club_members)) {
                    if($only_participant) {
                        foreach($club_members as $club_member) {
                            if(championship_stats_manager::getChampionshipStatsByChampionshipIdAndClubMemberId($championship_id, $club_member->id)) {
                                $output[1] = $output[1] + 1;
                            }
                        }
                    }
                    else {
                        $output[1] = $output[1] + count($club_members);
                    }
                }
            }
        }
        return $output;
    }

    public static function displaySwitchStatus(string $championship_id) {
        if(is_numeric($championship_id)) {
            $championship_id_int = (int)$championship_id;
            $championship = championship_manager::getChampionshipById($championship_id_int);
            if($championship) {
                $championship_settings = championship_settings_manager::getChampionshipSettingsByChampionshipId($championship_id_int);
                if($championship_settings) {
                    $championship_settings = unserialize($championship_settings->settings);
                    /// Utils
                    $championship_status = championship_status_manager::getChampionshipStatusByChampionshipId($championship_id_int);
                    /// Code
                    if(!$championship_status) {
                        /// Start
                        $members = championship_members_manager::getChampionshipMembersByChampionshipId($championship_id);
                        if($championship_settings->c > 0 && $championship_settings->m > 0 && $members && !empty($members)) {
                            $args = [
                                'championship_id' => $championship_id,
                                'conf' => 1,
                                'manche' => 1,
                                'finish' => 0
                            ];
                            $registered_clubs = championship_members_manager::getChampionshipMembersByChampionshipId($championship_id_int);
                            $registered_members = [];
                            if($registered_clubs && !empty($registered_clubs)) {
                                foreach($registered_clubs as $registered_club) {
                                    $club = club_manager::getClubById($registered_club->club_id);
                                    if($club && $club->participant == 1) {
                                        $club_members = club_member_manager::getClubMemberbyClub($club->id);
                                        if($club_members && !empty($club_members)) {
                                            foreach($club_members as $club_member) {
                                                if($club_member->participant != 0) {
                                                    $my_arg = [
                                                        'championship_id' => $championship_id,
                                                        'member_id' => $club_member->id,
                                                        'pts' => 0
                                                    ];
                                                    array_push($registered_members, $my_arg);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            if(count($registered_members) >= count($championship_settings->pts)) {
                                foreach($registered_members as $registered_member) {
                                    championship_stats_manager::registerChampionshipStats($registered_member);
                                }
                                championship_status_manager::registerChampionshipStatus($args);
                            }
                        }
                    }
                    else {
                        /// Reset
                        championship_manager::clearChampionshipDetails($championship_id_int);
                    }
                }
                mi_utils::adminRedirection("club_championship_db", ["status" => $championship_id]);
            }
        }
        mi_utils::adminRedirection("club_championship_db");
    }

    public static function displayChampionshipDynamicMembers(string $championship_id) {
        if(is_numeric($championship_id)) {
            $championship_id_int = (int)$championship_id;
            $championship = championship_manager::getChampionshipById($championship_id);
            if($championship) {
                /// Utils
                $members = self::getClubsWithPoints($championship_id, true);
                $members_count = $members ? count($members) : 0;
                /// Render
                echo '<h1>Championnat : ' .  $championship->nom .' ' . self::getToolBar($championship->id) . '</h1>';
                echo '<h2>Clubs participants (' . (string)$members_count .')</h2>';
                if($members_count > 0) {
                    echo '<table class="widefat fixed" cellspacing="0">';
                    echo '<tr>' .
                    '<th class="manage-column column-columnname " scope="col">Id Membre</th>' .
                        '<th class="manage-column column-columnname " scope="col">Id Club</th>' .
                        '<th class="manage-column column-columnname " scope="col">Nom</th>' .
                        '<th class="manage-column column-columnname " scope="col">Nb membres</th>' .
                        '<th class="manage-column column-columnname " scope="col">Nb membres participants</th>' .
                        '<th class="manage-column column-columnname " scope="col">Points</th>' .
                        '<th class="manage-column column-columnname " scope="col">Profil</th></tr>';
                    foreach ($members as $member) {
                        $selected_club = $member->club;
                        $selected_club_id = $selected_club ? $selected_club->id : '0';
                        $selected_club_total_lenght = club_member_manager::getClubMembersNumber((int)$selected_club->id);
                        echo '<tr style="border:1px solid #000">';
                        echo '<td>' . $member->member_id . '</td>';
                        echo '<td>' . $selected_club->id . '</td>';
                        if($selected_club) {
                            echo '<td><a title="Voir membres participants" href="?page=club_championship_db&d_members_members=' . $championship_id . '&club=' . $selected_club_id . '">' . ucfirst($selected_club->nom) . '</a></td>';
                        }
                        else {
                            echo '<td>Club inconnu</td>';
                        }
                        echo '<td>' . (string)$selected_club_total_lenght . '</td>';
                        echo '<td>' . $member->club_lenght . '</td>';
                        echo '<td>' . $member->pts . '</td>';
                        echo '<td><a title="Voir profil" href="?page=club_db&profil=' . $selected_club_id . '">Voir</a></td>';
                        echo '<td><a title="Editer club" href="?page=club_db&edit=' . $selected_club_id . '">✏️</a></td>';
                        echo '</tr>';
                    }
                    echo '</table>';
                }
                else {
                    echo '<h4>Ce championnat n' . "'" . 'a aucun club participant :/</h4>';
                }
            }
        }
    }

    public static function getClubsWithPoints(int $championship_id, bool $order = false): array {
        $output = [];
        $championship_members = championship_members_manager::getChampionshipMembersByChampionshipId($championship_id);
        if($championship_members && !empty($championship_members)) {
            foreach($championship_members as $championship_member) {
                $club = club_manager::getClubById($championship_member->club_id);
                if($club) {
                    $line = new championship_club_stat();
                    $line->club = $club;
                    $line->championship_id = $championship_id;
                    $line->member_id = $championship_member->id;
                    foreach(club_member_manager::getClubMemberbyClub($club->id) as $member) {
                        $championship_member_member = championship_stats_manager::getChampionshipStatsByChampionshipIdAndClubMemberId($championship_id, (int)$member->id);
                        if($championship_member_member && is_numeric($championship_member_member->pts)) {
                            $line->pts += $championship_member_member->pts;
                            $line->club_lenght += 1;
                        }
                    }
                    array_push($output, $line);
                }
            }
        }
        if($order && !empty($output)) {
            $temp = [];
            foreach($output as $value) {
                if(empty($temp)) {
                    array_push($temp, $value);
                }
                else if($value->pts >= $temp[0]->pts) {
                    mi_array_utils::shiftArray($temp, $value);
                }
                else {
                    $temp_ = [];
                    $temp__ = false;
                    for($i = 0; $i < count($temp); $i++) {
                        if($temp__) {
                            array_push($temp_, $temp[$i]);
                        }
                        else if($temp[$i]->pts < $value->pts) {
                            $temp__ = true;
                            array_push($temp_, $value);
                        }
                        array_push($temp_, $temp[$i]);
                        if(!$temp__ && $i >= count($temp) - 1) {
                            array_push($temp_, $value);
                        }
                    }
                    $temp = $temp_;
                }
            }
            $output = $temp;
        }
        return $output;
    }

    private static function getToolBar(string $championship_id): string {
        return '<a style="text-decoration:none;" title="Regler championnat" href="?page=club_championship_db&settings=' . $championship_id . '">⚙️</a>' .
        ' <a style="text-decoration:none;" title="Voir championnat" href="?page=club_championship_db&status=' . $championship_id. '">👁️</a>' .
        ' <a style="text-decoration:none;" title="Editer championnat" href="?page=club_championship_db&edit=' . $championship_id. '">✏️</a>' .
        ' <a style="text-decoration:none;" title="Voir membres" href="?page=club_championship_db&members=' . $championship_id . '">👥</a>';
    }

    public static function displayChampionshipDynamicClubMembers(string $championship_id, string $optional_club_id = '0') {
        if(is_numeric($championship_id)) {
            $championship_id_int = (int)$championship_id;
            $championship = championship_manager::getChampionshipById($championship_id);
            if($championship) {
                $members = null;
                $members_count = 0;
                $optional_member_exist = $optional_club_id != 0 && is_numeric($optional_club_id);
                $optional_club = null;
                $optional_club_member = null;
                $global_points = 0;
                /// Utils
                $members = self::getChampionshipMembersMembersPoints($championship_id, ($optional_member_exist ? (int)$optional_club_id : 0), true);
                if($optional_member_exist) {
                    $optional_club = club_manager::getClubById((int)$optional_club_id);
                    $optional_club_member = championship_members_manager::getChampionshipMemberByClubIdAndChampionshipId((int)$optional_club_id, $championship_id_int);
                    foreach($members as $member) {
                        $global_points += $member->pts;
                    }
                }
                $members_count = $members ? count($members) : 0;
                /// Render
                echo '<h1>Championnat : ' .  $championship->nom .' ' . self::getToolBar($championship->id) . '</h1>';
                if($optional_member_exist) {
                    echo '<h3>Club: ' . ($optional_club ? $optional_club->nom : 'Club inconnu') . '</h3>';
                    echo '<h4>Id membre: ' . ($optional_club_member ? $optional_club_member->club_id : '?') . '<br>';
                    echo 'Points: ' . ($global_points) . '<br>';
                    echo 'Profil: ' . ($optional_club ? '<a title="Voir profil" href="?page=club_db&profil=' . $optional_club->id . '">Voir</a>' : '?') . '</h4>';
                }
                echo '<h2>Membres participants (' . (string)$members_count .')</h2>';
                if($members_count > 0) {
                    echo '<table class="widefat fixed" cellspacing="0">';
                    echo '<tr>' .
                    '<th class="manage-column column-columnname " scope="col">Id</th>' .
                        '<th class="manage-column column-columnname " scope="col">Num Adhérent</th>' .
                        '<th class="manage-column column-columnname " scope="col">Nom</th>' .
                        '<th class="manage-column column-columnname " scope="col">Prenom</th>' .
                        (!$optional_member_exist ? '<th class="manage-column column-columnname " scope="col">Club</th>': '') .
                        '<th class="manage-column column-columnname " scope="col">Points</th>' .
                        '<th class="manage-column column-columnname " scope="col">Profil</th></tr>';
                    foreach ($members as $member) {
                        $selected_member= $member->member;
                        $selected_member_id = $selected_member ? $selected_member->id : '0';
                        echo '<tr style="border:1px solid #000">';
                        echo '<td>' . $selected_member->id . '</td>';
                        echo '<td>' . $selected_member->adh . '</td>';
                        echo '<td>' . $selected_member->nom . '</td>';
                        echo '<td>' . $selected_member->prenom . '</td>';
                        if(!$optional_member_exist) {
                            if($selected_member->club) {
                                echo '<td><a title="Voir membres participants" href="?page=club_championship_db&d_members_members=' . $championship_id . '&club=' . $member->club->id . '">' . ucfirst($member->club->nom) . '</a></td>';
                            }
                            else if(!$selected_member->has_club) {
                                echo '<td>Aucun club</td>';
                            }
                            else {
                                echo '<td>Club inconnu</td>';
                            }
                        }
                        echo '<td>' . $member->pts . '</td>';
                        echo '<td><a title="Voir profil" href="?page=club_member_db&profil=' . $selected_member_id . '">Voir</a></td>';
                        echo '<td><a title="Editer membre" href="?page=club_member_db&edit=' . $selected_member_id . '">✏️</a></td>';
                        echo '</tr>';
                    }
                    echo '</table>';
                }
                else {
                    echo '<h4>Ce ' . ($optional_member_exist ? 'club' : 'championnat') . ' n' . "'" . 'a aucun club participant :/</h4>';
                }
            }
        }
    }

    public static function getChampionshipMembersMembersPoints(int $championship_id, int $club_member_id = 0, bool $order = false): array {
        $output = [];
        $clubs = club_manager::getClubs();
        $championship_members = championship_stats_manager::getChampionshipStatsByChampionshipId($championship_id);
        if($championship_members && !empty($championship_members)) {
            foreach($championship_members as $championship_member) {
                $flag = $club_member_id == 0;
                if(!$flag) {
                    $temp = club_member_manager::getClubMemberById($championship_member->member_id);
                    if($temp) {
                        $flag = $club_member_id == $temp->club;
                    }
                }
                if($flag) {
                    $line = new championship_club_member_stat();
                    $line->member_id = $championship_member->id;
                    $line->pts = $championship_member->pts;
                    $line->championship_id = $championship_member->championship_id;
                    $line->member = club_member_manager::getClubMemberById((int)$championship_member->member_id);
                    if($line->member) {
                        if($line->member->club != 0) {
                            $line->has_club = true;
                            foreach($clubs as $club) {
                                if($club->id == $line->member->club) {
                                    $line->club = $club;
                                    break;
                                }
                            }
                        }
                        array_push($output, $line);
                    }
                }
            }
        }
        if(!empty($output) && $order) {
            $temp = [];
            foreach($output as $value) {
                if(empty($temp)) {
                    array_push($temp, $value);
                }
                else if($value->pts >= $temp[0]->pts) {
                    mi_array_utils::shiftArray($temp, $value);
                }
                else {
                    $temp_ = [];
                    $temp__ = false;
                    for($i = 0; $i < count($temp); $i++) {
                        if($temp__) {
                            array_push($temp_, $temp[$i]);
                        }
                        else if($temp[$i]->pts < $value->pts) {
                            $temp__ = true;
                            array_push($temp_, $value);
                        }
                        array_push($temp_, $temp[$i]);
                        if(!$temp__ && $i >= count($temp) - 1) {
                            array_push($temp_, $value);
                        }
                    }
                    $temp = $temp_;
                }
            }
            $output = $temp;
        }
        return $output;
    }

    public static function displayResultEditor(string $championship_id) {
        if(is_numeric($championship_id)) {
            $championship_id_int = (int)$championship_id;
            
            $championship = championship_manager::getChampionshipById($championship_id_int);
            if($championship) {
                // Utils
                $status = championship_status_manager::getChampionshipStatusByChampionshipId($championship_id_int);
                $current_settings = championship_settings_manager::getChampionshipSettingsByChampionshipId($championship_id_int);
                $current_settings = $current_settings ? unserialize($current_settings->settings) : null;
                if($status && $current_settings && $status->finish == 0) {
                    /// Update results
                    if(mi_array_utils::containKeyPair($_POST, "update", "type")) {
                        $ad_mode = $_POST['mode'] == 'ad';
                        $players = [];
                        $success = true;
                        $classement_size = count($current_settings->pts);
                        for($i = 0; $i < $classement_size; $i++) {
                            $current_id_ = "score_pts_". ($i+1);
                            if(mi_array_utils::arrayContain($_POST, $current_id_, true, true) && is_numeric($_POST[$current_id_])) {
                                $member = $ad_mode ? club_member_manager::getClubMemberByNumAdh((int)$_POST[$current_id_]) : club_member_manager::getClubMemberById((int)$_POST[$current_id_]);
                                $stat = $member ? championship_stats_manager::getChampionshipStatsByChampionshipIdAndClubMemberId($championship_id_int, (int)$member->id) : null;
                                if($stat && !mi_array_utils::arrayContainObjectField($players, 'member_id', $member->id)) {
                                    array_push($players, $stat);
                                    continue;
                                }
                            }
                            $success = false;
                            break;
                        }
                        if($success && count($players) == $classement_size) {
                            for($i = 0; $i < $classement_size; $i++) {
                                $score = ((int)$players[$i]->pts + (int)$current_settings->pts[$i]);
                                championship_stats_manager::updateChampionshipStatsById((int)$players[$i]->id, ["pts" => $score, "member_id" => (int)$players[$i]->member_id, "championship_id" => $championship_id]);
                            }
                            $status_args = ["championship_id" => $championship_id, "conf" => (int)$status->conf, "manche" => (int)$status->manche, "finish" => (int)$status->finish];
                            if($status->manche == $current_settings->m && $status->conf == $current_settings->c) {
                                $status_args['finish'] = 1;
                            }
                            else if($status->manche == $current_settings->m) {
                                $status_args['manche'] = 1;
                                $status_args['conf'] = (int)$status_args['conf'] + 1;
                            }
                            else {
                                $status_args['manche'] = (int)$status_args['manche'] + 1;
                            }
                            $ids = [];
                            foreach($players as $player) {
                                array_push($ids, (int)$player->member_id);
                            }
                            championship_results_manager::registerChampionshipResult(['championship_id' => $championship_id, 'conf' => (int)$status->conf, 'manche' => $status->manche, "classement" => serialize($ids)]);
                            championship_status_manager::updateChampionshipStatusByChampionshipId($championship_id_int, $status_args);
                            mi_utils::adminRedirection('club_championship_db', ['status' => $championship_id]);
                            return;
                        }
                        echo "<h4>Une erreur est survenu lors de la mise à jour des résultats :/</h4>";
                    }
                    //Render
                    echo '<h1>Championnat n°' . $championship_id . ' : ' . $championship->nom . ' ' . self::getToolBar($championship_id_int) . '</h1>';
                    echo '<h4>Détail:<br>Confrontation: (' . $status->conf . '/' . $current_settings->c . ')' . ($status->finish != 0 ? ' (Terminé)' : '') . '<br>Manche: (' . $status->manche . '/' . $current_settings->m . ')</h4>';
                    echo '<h3>Mettre à jour les resultats</h3>';
                    echo '<form action="" method="post" style="padding-right: 15px;">';
                    echo '<input type="hidden" name="type" value="update">';
                    echo '<p>';
                    echo '<label for="mode">Mode : </label>';
                    echo '<select id="mode" name="mode">';
                    echo '<option value="id">ID</option>';
                    echo '<option value="ad">Numéro adhérent</option>';
                    echo '</select>';
                    echo '<div id="pts_score_main_div" style="background-color: #FFF; padding: 15px; width: 15%;">';
                    echo '<label id="' . $add_label_name . '">Classement: (' . count($current_settings->pts) . ' places)</label>';
                    for($i = 0; $i < count($current_settings->pts); $i++) {
                        $current_id_ = $i + 1;
                        echo '<div style="margin-top: 10px; padding: 5px; border-radius: 5px;">';
                        echo '<label>n°' . $current_id_ . ' (+' . $current_settings->pts[$i] . 'pts)</label> <br>';
                        echo '<input type="text" name="score_pts_' . $current_id_ . '" style="display: block; width: inherit; display: inline-block;">';
                        echo '</div>';
                    }
                    echo '</div>';
                    echo '<p>';
                    echo '<input type="submit" value="Enregister"></p>';
                    echo '</form>';
                    return;
                }
            }
        }
        mi_utils::adminRedirection("club_championship_db");
    }

    public static function displayResults(string $championship_id) {
        if(is_numeric($championship_id)) {
            $championship_id_int = (int)$championship_id;
            
            $championship = championship_manager::getChampionshipById($championship_id_int);
            if($championship) {
                // Utils
                $status = championship_status_manager::getChampionshipStatusByChampionshipId($championship_id_int);
                $current_settings = championship_settings_manager::getChampionshipSettingsByChampionshipId($championship_id_int);
                $current_settings = $current_settings ? unserialize($current_settings->settings) : null;
                $clubs = club_manager::getClubsWithName();
                if($status && $current_settings) {
                    //Render
                    echo '<h1>Championnat n°' . $championship_id . ' : ' . $championship->nom . ' ' . self::getToolBar($championship_id_int) . '</h1>';
                    echo '<h4>Détail:<br>Confrontation: (' . $status->conf . '/' . $current_settings->c . ')' . ($status->finish != 0 ? ' (Terminé)' : '') . '<br>Manche: (' . $status->manche . '/' . $current_settings->m . ')</h4>';
                    echo '<h3>Resultats:</h3>';
                    for($i = 0; $i < (int)$status->conf; $i++) {
                        $conf = ($i+1);
                        $results = championship_results_manager::getChampionshipResultsByChampionshipIdAndConf($championship_id_int, $conf);
                        if($results && !empty($results)) {
                            echo '<div style="background-color: #FFF; padding: 10px; margin-bottom: 10px; margin-right: 10px;">';
                            echo '<h3>Confrontation n°' . (string)$conf . '</h3>';
                            foreach ($results as $line) {
                                echo '<table class="widefat fixed" cellspacing="0">';
                                echo '<tr>Manche n°' . $line->manche . '</tr>';
                                echo '<tr>' .
                                    '<th class="manage-column column-columnname " scope="col">Place</th>' .
                                    '<th class="manage-column column-columnname " scope="col">Id</th>' .
                                    '<th class="manage-column column-columnname " scope="col">Num Adhérent</th>' .
                                    '<th class="manage-column column-columnname " scope="col">Nom</th>' .
                                    '<th class="manage-column column-columnname " scope="col">Prenom</th>' .
                                    '<th class="manage-column column-columnname " scope="col">Club</th>' .
                                    '<th class="manage-column column-columnname " scope="col">Profil</th></tr>';
                                $classement = unserialize($line->classement);
                                if($classement && !empty($classement)) {
                                    for($a = 0; $a < count($classement); $a++) {
                                        $member = club_member_manager::getClubMemberById((int)$classement[$a]);
                                        echo '<tr style="border:1px solid #000">';
                                        echo '<td>' . ($a+1) . '</td>';
                                        echo '<td>' . $classement[$a] . '</td>';
                                        if($member) {
                                            echo '<td>' . $member->adh . '</td>';
                                            echo '<td>' . $member->nom . '</td>';
                                            echo '<td>' . $member->prenom . '</td>';
                                            if(mi_array_utils::arrayContain($clubs, (string)$member->club, true, true)) {
                                                echo '<td>';
                                                echo '<a title="Voir membres participants" href="?page=club_championship_db&d_members_members=' . $championship_id . '&club=' . $member->club . '">' . $clubs[$member->club] . '</a>';
                                                echo '</td>';
                                            }
                                            else if($member->club == 0) {
                                                echo '<td>Aucun club</td>';
                                            }
                                            else {
                                                echo '<td>Club inconnu</td>';
                                            }
                                            echo '<td><a title="Voir profil" href="?page=club_member_db&profil=' . $member->id . '">Voir</a></td>';
                                        }
                                        echo '</tr>';
                                    }
                                }
                                echo '</table>';
                            }
                            echo '</div>';
                        }
                    }
                    return;
                }
            }
        }
        mi_utils::adminRedirection("club_championship_db");
    }
}