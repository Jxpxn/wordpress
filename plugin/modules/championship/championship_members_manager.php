<?php
class championship_members_manager {
    public static function registerChampionshipMember(array $championship): bool {
        global $wpdb;
        if(mi_array_utils::arrayHasRequirements($championship, mi_config::$club_championship_members_requirements)) {
            $data = mi_array_utils::arrayExtract($championship, mi_config::$club_championship_members_requirements);
            if($data['club_id'] != 0 && !self::getChampionshipMemberByClubIdAndChampionshipId($data['club_id'], $data['championship_id'])) {
                $wpdb->insert($wpdb->prefix.mi_config::$mi_clubs_championship_members_db_name, $data);
                return true;
            }
        }
        return false;
    }

    public static function getChampionshipMemberById(int $id) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_members_db_name . " WHERE id=" . (string)$id . ";");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function getChampionshipMemberByClubIdAndChampionshipId(int $club_id, int $championship_id) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_members_db_name . " WHERE club_id=" . (string)$club_id . " AND championship_id=" . (string)$championship_id . ";");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function getChampionshipMembersByClubId(int $id) {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_members_db_name . " WHERE club_id=" . (string)$id . ";");
    }

    public static function getChampionshipMembersByChampionshipId(int $id) {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_members_db_name . " WHERE championship_id=" . (string)$id . ";");
    }

    public static function deleteChampionshipMemberById(int $id): void {
        global $wpdb;
        $wpdb->delete($wpdb->prefix . mi_config::$mi_clubs_championship_members_db_name, array("id" => $id));
    }

    public static function deleteChampionshipMembersByClubId(int $id): void {
        global $wpdb;
        $wpdb->delete($wpdb->prefix . mi_config::$mi_clubs_championship_members_db_name, array("club_id" => $id));
    }

    public static function deleteChampionshipMembersByChampionshipId(int $id): void {
        global $wpdb;
        $wpdb->delete($wpdb->prefix . mi_config::$mi_clubs_championship_members_db_name, array("championship_id" => $id));
    }

    public static function deleteChampionshipMembersByClubIdAndChampionshipId(int $club_id, int $championship_id): void {
        global $wpdb;
        $wpdb->delete($wpdb->prefix . mi_config::$mi_clubs_championship_members_db_name, array("club_id" => $club_id, "championship_id" => $championship_id));
    }

    public static function getChampionshipMembers() {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_members_db_name . ";");
    }

    public static function getChampionshipMembersCount(int $championship_id): int {
        global $wpdb;
        $output = 0;
        $result = $wpdb->get_results("SELECT COUNT(`id`) as total FROM " . $wpdb->prefix . mi_config::$mi_clubs_championship_members_db_name . " WHERE championship_id=" . (string)$championship_id . ";");
        if($result && !empty($result)) {
            $output = is_numeric($result[0]->total) ? (int)$result[0]->total : 0;
        }
        return $output;
    }
}