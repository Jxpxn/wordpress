<?php
class club_category_display {
    public static function displayTable() {
        echo "<h1>".get_admin_page_title()."</h1>";
        echo "<form method='post' id='club_db_form'>";
        echo '<input type="hidden" name="type" value="delete">';
        echo '<table class="widefat fixed" cellspacing="0">';
        echo '<tr><th class="manage-column column-columnname " scope="col">Select</th>' .
            '<th class="manage-column column-columnname " scope="col">Id</th>' .
            '<th class="manage-column column-columnname " scope="col">Nom</th>' .
            '<th class="manage-column column-columnname " scope="col">Membres</th></tr>';
        foreach (club_category_manager::getClubsCategory() as $line) {
            echo '<tr style="border:1px solid #000">';
            echo '<td><input class="widefat" type="checkbox" name="selected_category_' . $line->id . '" value="y" /></td>';
            echo '<td>' . $line->id . '</td>';
            echo '<td>' . $line->nom . '</td>';
            echo '<td><a title="Voir profil" href="?page=club_category_db&members=' . $line->id . '">Voir</a></td>';
            echo '<td><a title="Editer catégorie" href="?page=club_category_db&edit=' . $line->id . '">✏️</a></td>';
            echo '</tr>';
        }
        echo '<tr><th class="manage-column column-columnname " scope="col"><label onclick="document.getElementById(`club_db_form`).submit();">Supprimer</label></th></tr>';
        echo '</table>';
        echo '</form>';
    }

    public static function displayAdder() {
        if(mi_array_utils::containKeyPair($_POST, "add", "type")) {
            if(club_category_manager::registerCategory($_POST)) {
                mi_utils::adminRedirection('club_category_db');
                return;
            }
            else {
                echo "<h1>Une erreur est survenu lors de l'inscription de la catégorie :/</h1>";
            }
        }

        echo "<h1>".get_admin_page_title()."</h1>";

        echo '<form action="" method="post" style="padding-right: 15px;">';
        echo '<input type="hidden" name="type" value="add">';
        echo '<p>';
        echo '<label for="nom">Nom :</label>';
        echo '<input class="widefat" id="nom" name="nom" type="text" value="" />';
        echo '</p>';
        echo '<p><input type="submit" value="Enregister"></p>';
        echo '</form>';
    }

    public static function displayDelete(array $categorys) {
        $todelete = [];
        foreach($categorys as $key => $value) {
            if($value === 'y') {
                $parsed = explode('selected_category_', $key);
                $parsed = $parsed[count($parsed)-1];
                if(is_numeric($parsed)) {
                    array_push($todelete, $parsed);
                }
            }
        }
        foreach($todelete as $club) {
            $clubs = club_manager::getClubsByCategory((int)$club);
            if(!empty($clubs)) {
                foreach($clubs as $cluby) {
                    club_manager::updateClubCategoryById($cluby->id, 0);
                }
            }
            $championships = championship_manager::getChampionshipsByCategory((int)$club);
            if($championships && !empty($championships)) {
                foreach($championships as $championship) {
                    if($championship->category != 0) {
                        championship_manager::clearChampionshipDetails((int)$championship->id);
                        championship_manager::updateChampionshipCategoryById((int)$championship->id, 0);
                    }
                }
            }
            club_category_manager::deleteClubCategoryById((int)$club);
        }
        self::displayTable();
    }

    public static function displayConfEdit(string $category_id, array $args) {
        if(is_numeric($category_id)) {
            $category_id_int = (int)$category_id;
            $category = club_category_manager::getClubCategoryById($category_id_int);
            if(!$category || !club_category_manager::updateClubCategoryById($category_id_int, $args)) {
                echo "Erreur lors de l'enregistrement de la catégorie :/";
                self::displayEditor($category_id);
                return;
            }
        }
        mi_utils::adminRedirection('club_category_db');
    }

    public static function displayEditor(string $category_id) {
        if(is_numeric($category_id)) {
            $category_id_int = (int)$category_id;
            $category = club_category_manager::getClubCategoryById($category_id_int);
            if($category) {
                echo "<h1>".get_admin_page_title()."</h1>";
                echo '<h2>Categorie n°' . $category->id . '</h2>';
                echo '<form action="" method="post" style="padding-right: 15px;">';
                echo '<input type="hidden" name="conf_edit" value="' . $category->id . '">';
                echo '<p>';
                echo '<label for="nom">Nom :</label>';
                echo '<input class="widefat" id="nom" name="nom" type="text" value="' . $category->nom . '" />';
                echo '</p>';
                echo '<p><input type="submit" value="Enregister"></p>';
                echo '</form>';
            }
        }
        else {
            club_display::displayTable();
        }
    }

    public static function displayMembers(string $category_id) {
        if(is_numeric($category_id)) {
            $category_id_num = (int)$category_id;
            $category = club_category_manager::getClubCategoryById($category_id_num);
            if($category) {
                $members = club_manager::getClubsByCategory($category_id_num);
                $members_count = $members ? count($members) : 0;
                echo '<h1>Catégorie: ' .  $category->nom .'</h1>';
                echo '<h2>Clubs (' . (string)$members_count .')</h2>';
                if($members_count > 0) {
                    echo '<table class="widefat fixed" cellspacing="0">';
                    echo '<tr>' .
                        '<th class="manage-column column-columnname " scope="col">Id</th>' .
                        '<th class="manage-column column-columnname " scope="col">Nom</th>' .
                        '<th class="manage-column column-columnname " scope="col">Profil</th></tr>';
                    foreach ($members as $line) {
                        echo '<tr style="border:1px solid #000">';
                        echo '<td>' . $line->id . '</td>';
                        echo '<td>' . ucfirst($line->nom) . '</td>';
                        echo '<td><a title="Voir profil" href="?page=club_member_db&profil=' . $line->id . '">Voir</a></td>';
                        echo '<td><a title="Editer club" href="?page=club_db&edit=' . $line->id . '">✏️</a></td>';
                        echo '</tr>';
                    }
                    echo '</table>';
                }
                else {
                    echo '<h4>Cette catégorie n' . "'" . 'a aucun club membre :/</h4>';
                }
                $championship = championship_manager::getChampionshipsByCategory($category_id_num);
                $championship_count = $championship ? count($championship) : 0;
                echo '<h2>Championnats (' . (string)$championship_count .')</h2>';
                if($championship_count > 0) {
                    echo '<table class="widefat fixed" cellspacing="0">';
                    echo '<tr>' .
                        '<th class="manage-column column-columnname " scope="col">Id</th>' .
                        '<th class="manage-column column-columnname " scope="col">Nom</th>' .
                        '<th class="manage-column column-columnname " scope="col">Membres</th></tr>';
                    foreach ($championship as $line) {
                        echo '<tr style="border:1px solid #000">';
                        echo '<td>' . $line->id . '</td>';
                        echo '<td>' . ucfirst($line->nom) . '</td>';
                        echo '<td><a title="Voir profil" href="?page=club_championship_db&members=' . $line->id . '">Voir</a></td>';
                        echo '<td><a title="Editer club" href="?page=club_championship_db&edit=' . $line->id . '">✏️</a></td>';
                        echo '</tr>';
                    }
                    echo '</table>';
                }
                else {
                    echo '<h4>Cette catégorie n' . "'" . 'a aucun championnat membre :/</h4>';
                }
            }
        }
    }

}