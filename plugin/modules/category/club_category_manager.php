<?php
class club_category_manager {
    public static function registerCategory(array $category): bool {
        global $wpdb;
        if(mi_array_utils::arrayHasRequirements($category, mi_config::$club_category_requirements)) {
            $data = mi_array_utils::arrayExtract($category, mi_config::$club_category_requirements);
            if(!self::getClubCategoryByName($data['nom'])) {
                $wpdb->insert($wpdb->prefix.mi_config::$mi_clubs_category_db_name, $data);
                return true;
            }
        }
        return false;
    }

    public static function getClubCategoryByName(string $name) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_category_db_name . " WHERE nom='" . $name . "';");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function getClubCategoryById(int $id) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_category_db_name . " WHERE id=" . (string)$id . ";");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function getClubsCategory() {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_category_db_name . ";");
    }

    public static function deleteClubCategoryById(int $id): void {
        global $wpdb;
        $wpdb->delete($wpdb->prefix . mi_config::$mi_clubs_category_db_name, array("id" => $id));
    }

    public static function updateClubCategoryById(int $id, array $category): bool {
        global $wpdb;
        if(mi_array_utils::arrayHasRequirements($category, mi_config::$club_category_requirements)) {
            $data = mi_array_utils::arrayExtract($category, mi_config::$club_category_requirements);
            $confirm = self::getClubCategoryByName($data['nom']);
            if($data && (!$confirm || $confirm->id == $id)) {
                $wpdb->update($wpdb->prefix.mi_config::$mi_clubs_category_db_name, $data, ['id' => $id]);
                return true;
            }
        }
        return false;
    }
}