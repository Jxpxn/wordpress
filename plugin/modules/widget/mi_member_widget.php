<?php

class class_wp_mi_member_widget extends WP_Widget {
    public function __construct()
    {
        $widget_opts = [
            'classname' => 'widget_mi_member',
            'description' => __('Widget de stat'),
            'customize_selective_refresh' => true,
        ];

        parent::__construct('mim',__('MI Member Stats','MIM'),$widget_opts);
    }

    public function form($instance)
    {

        $instance = wp_parse_args( (array)$instance,
            array() );
        ?>
        <p>
            Not editable :/
        </p>
        <?php
    }

    public function update($new_instance, $old_instance)
    {
        $intance = $old_instance;
        return $intance;
    }

    public function widget($args, $instance)
    {
        ///$title = 'Statistiques';
        echo $args['before_widget'];
        /*if ( $title ) {
            echo $args['before_title'] . $title . $args['after_title'];
        }*/
        echo '<div id="mi_wrap" class="mi_wrap">';
        echo '<div style="background-color: rgba(255, 255, 255, 0.5); padding: 2px; margin-bottom: 10px; margin-right: 10px; border-radius: 5px;">';
        echo '<h3>Membres</h3>';
        echo '<table class="table" cellspacing="0">';
        echo '<tr>' .
            '<th class="manage-column column-columnname " scope="col">Dep</th>' .
            '<th class="manage-column column-columnname " scope="col">Pourcentage</th>' .
            '<th class="manage-column column-columnname " scope="col">Nombre</th>' . '</tr>';
        $deps = [];
        $clubs = club_member_manager::getClubsMember();
        if($clubs && !empty($clubs)) {
            foreach($clubs as $club) {
                $dep = substr($club->cp, 0, 2);
                if(isset($deps[$dep])) {
                    $deps[$dep] = (int)$deps[$dep] + 1;
                }
                else {
                    $deps[$dep] = 1;
                }
            }
        }
        if(!empty($deps)) {
            $total_length = 0;
            foreach($deps as $key => $value) {
                $total_length += (int)$value;
            }
            foreach($deps as $key => $value) {
                $perc = ((float)(100.0*(int)$value)/$total_length);
                $perc = strlen($perc) > 5 ? substr($perc, 0, 5) : $perc;
                echo '<tr><td>' . $key . '</td><td>' . $perc . '%</td><td>' . $value . '</td></tr>';
            }
        }
        else {
            echo '<tr><td colspan="3">Aucun membre :/</td></tr>';
        }
        echo '</table>';
        echo '</div>';
        echo '</div>';
        echo $args['after_widget'];

        return '';
    }
}