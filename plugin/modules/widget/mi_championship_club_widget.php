<?php

class class_wp_mi_championship_club_widget extends WP_Widget {
    public function __construct()
    {
        $widget_opts = [
            'classname' => 'widget_mi_championship_club',
            'description' => __('Widget de stat'),
            'customize_selective_refresh' => true,
        ];

        parent::__construct('micc',__('MI Championships Clubs Score','MICC'),$widget_opts);
    }

    public function form($instance)
    {

        $instance = wp_parse_args( (array)$instance,
            array() );
        ?>
        <p>
            Not editable :/
        </p>
        <?php
    }

    public function update($new_instance, $old_instance)
    {
        $intance = $old_instance;
        return $intance;
    }

    public function widget($args, $instance)
    {
        ///$title = 'Statistiques';
        echo $args['before_widget'];
        /*if ( $title ) {
            echo $args['before_title'] . $title . $args['after_title'];
        }*/
        echo '<div id="mi_wrap" class="mi_wrap">';
        echo '<div style="background-color: rgba(255, 255, 255, 0.5); padding: 2px; margin-bottom: 10px; margin-right: 10px; border-radius: 5px;">';
        echo '<h3>Championnat Clubs</h3>';
        $championships = championship_manager::getChampionships();
        $loop_count = 0;
        if($championships && !empty($championships)) {
            $confirm = false;
            foreach($championships as $championship) {
                if($loop_count >= 3) { break; }
                $championship_id_int = (int)$championship->id;
                $status = championship_status_manager::getChampionshipStatusByChampionshipId((int)$championship->id);
                $current_settings = championship_settings_manager::getChampionshipSettingsByChampionshipId($championship_id_int);
                $current_settings = $current_settings ? unserialize($current_settings->settings) : null;
                if($status && $current_settings) {
                    $confirm = true;
                    $loop_count += 1;
                    $members = championship_display::getClubsWithPoints((int)$championship->id, true);
                    echo '<table class="widefat fixed" cellspacing="0" style="border:1px solid #000; border-radius: 5px; margin-bottom: 5px;">';
                    echo '<tr><th colspan="4">' . $championship->nom . '</th></tr>';
                    echo '<tr><th colspan="4">' . ($status->finish != 0 ? '(Terminer)' : 'Confrontation (' . $status->conf . '/' . $current_settings->c . ') Manche (' . $status->manche . '/' . $current_settings->m . ')') . '</th></tr>';
                    echo '<tr>' .
                    '<th class="manage-column column-columnname " scope="col">Place</th>' .
                        '<th class="manage-column column-columnname " scope="col">Nom</th>' .
                        '<th class="manage-column column-columnname " scope="col">Nb membres</th>' .
                        '<th class="manage-column column-columnname " scope="col">Points</th></tr>';
                    if($members) {
                        $max_length = count($members) > 3 ? 3 : count($members);
                        for($i = 0; $i < $max_length; $i++) {
                            $member = $members[$i];
                            $selected_club = $member->club;
                            $selected_club_id = $selected_club ? $selected_club->id : '0';
                            $selected_club_total_lenght = club_member_manager::getClubMembersNumber((int)$selected_club->id);
                            echo '<tr style="border:1px solid #000">';
                            echo '<td>' . ($i+1) . '</td>';
                            if($selected_club) {
                                echo '<td>' . ucfirst($selected_club->nom) . '</td>';
                            }
                            else {
                                echo '<td>Club inconnu</td>';
                            }
                            echo '<td>' . $member->club_lenght . '</td>';
                            echo '<td>' . $member->pts . '</td>';
                            echo '</tr>';
                        }
                    }
                    else {
                        echo '<tr colspan="4"><td>Aucun membre :/</td></tr>';
                    }
                    echo '</table>';
                }
            }
            if(!$confirm) {
                echo '<h4>Aucun championnat en cours :/</h4>';
            }
        }
        else {
            echo '<h4>Aucun championnat en cours :/</h4>';
        }
        echo '</div>';
        echo '</div>';
        echo $args['after_widget'];

        return '';
    }
}