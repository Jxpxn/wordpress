<?php
class stats_display {
    public static function display() {
        echo "<h1>".get_admin_page_title()."</h1>";
        // Clubs
        echo '<div style="background-color: #FFF; padding: 10px; margin-bottom: 10px; margin-right: 10px;">';
        echo '<h3>Liste club par département</h3>';
        echo '<table class="widefat fixed" cellspacing="0">';
        echo '<tr>' .
            '<th class="manage-column column-columnname " scope="col">Dep</th>' .
            '<th class="manage-column column-columnname " scope="col">Pourcentage</th>' .
            '<th class="manage-column column-columnname " scope="col">Nombre</th>' . '</tr>';
        $deps = [];
        $clubs = club_manager::getClubs();
        if($clubs && !empty($clubs)) {
            foreach($clubs as $club) {
                $dep = substr($club->cp, 0, 2);
                if(isset($deps[$dep])) {
                    $deps[$dep] = (int)$deps[$dep] + 1;
                }
                else {
                    $deps[$dep] = 1;
                }
            }
        }
        if(!empty($deps)) {
            $total_length = 0;
            foreach($deps as $key => $value) {
                $total_length += (int)$value;
            }
            foreach($deps as $key => $value) {
                echo '<tr><td>' . $key . '</td><td>' . (string)((100.0*(int)$value)/$total_length) . '%</td><td>' . $value . '</td></tr>';
            }
        }
        else {
            echo '<tr><td>Aucun club :/</td></tr>';
        }
        echo '</table>';
        // Members
        echo '</div>';
        echo '<div style="background-color: #FFF; padding: 10px; margin-bottom: 10px; margin-right: 10px;">';
        echo '<h3>Liste membre par département</h3>';
        echo '<table class="widefat fixed" cellspacing="0">';
        echo '<tr>' .
            '<th class="manage-column column-columnname " scope="col">Dep</th>' .
            '<th class="manage-column column-columnname " scope="col">Pourcentage</th>' .
            '<th class="manage-column column-columnname " scope="col">Nombre</th>' . '</tr>';
        $deps = [];
        $clubs = club_member_manager::getClubsMember();
        if($clubs && !empty($clubs)) {
            foreach($clubs as $club) {
                $dep = substr($club->cp, 0, 2);
                if(isset($deps[$dep])) {
                    $deps[$dep] = (int)$deps[$dep] + 1;
                }
                else {
                    $deps[$dep] = 1;
                }
            }
        }
        if(!empty($deps)) {
            $total_length = 0;
            foreach($deps as $key => $value) {
                $total_length += (int)$value;
            }
            foreach($deps as $key => $value) {
                echo '<tr><td>' . $key . '</td><td>' . (string)((100.0*(int)$value)/$total_length) . '%</td><td>' . $value . '</td></tr>';
            }
        }
        else {
            echo '<tr><td>Aucun membre :/</td></tr>';
        }
        echo '</table>';
        echo '</div>';
        // Championships Clubs
        echo '<div style="background-color: #FFF; padding: 10px; margin-bottom: 10px; margin-right: 10px;">';
        echo '<h3>Classement des championnat (Clubs)</h3>';
        $championships = championship_manager::getChampionships();
        if($championships && !empty($championships)) {
            $confirm = false;
            foreach($championships as $championship) {
                $status = championship_status_manager::getChampionshipStatusByChampionshipId((int)$championship->id);
                if($status) {
                    $confirm = true;
                    $members = championship_display::getClubsWithPoints((int)$championship->id, true);
                    echo '<table class="widefat fixed" cellspacing="0">';
                    echo '<tr><th>' . $championship->nom . '</th></tr>';
                    echo '<tr>' .
                    '<th class="manage-column column-columnname " scope="col">Place</th>' .
                    '<th class="manage-column column-columnname " scope="col">Id Membre</th>' .
                        '<th class="manage-column column-columnname " scope="col">Id Club</th>' .
                        '<th class="manage-column column-columnname " scope="col">Nom</th>' .
                        '<th class="manage-column column-columnname " scope="col">Nb membres</th>' .
                        '<th class="manage-column column-columnname " scope="col">Nb membres participants</th>' .
                        '<th class="manage-column column-columnname " scope="col">Points</th>' .
                        '<th class="manage-column column-columnname " scope="col">Profil</th></tr>';
                    if($members) {
                        $max_length = count($members) > 3 ? 3 : count($members);
                        for($i = 0; $i < $max_length; $i++) {
                            $member = $members[$i];
                            $selected_club = $member->club;
                            $selected_club_id = $selected_club ? $selected_club->id : '0';
                            $selected_club_total_lenght = club_member_manager::getClubMembersNumber((int)$selected_club->id);
                            echo '<tr style="border:1px solid #000">';
                            echo '<td>' . ($i+1) . '</td>';
                            echo '<td>' . $member->member_id . '</td>';
                            echo '<td>' . $selected_club->id . '</td>';
                            if($selected_club) {
                                echo '<td><a title="Voir membres participants" href="?page=club_championship_db&d_members_members=' . $championship_id . '&club=' . $selected_club_id . '">' . ucfirst($selected_club->nom) . '</a></td>';
                            }
                            else {
                                echo '<td>Club inconnu</td>';
                            }
                            echo '<td>' . (string)$selected_club_total_lenght . '</td>';
                            echo '<td>' . $member->club_lenght . '</td>';
                            echo '<td>' . $member->pts . '</td>';
                            echo '<td><a title="Voir profil" href="?page=club_db&profil=' . $selected_club_id . '">Voir</a></td>';
                            echo '<td><a title="Editer club" href="?page=club_db&edit=' . $selected_club_id . '">✏️</a></td>';
                            echo '</tr>';
                        }
                    }
                    else {
                        echo '<tr><td>Aucun membre :/</td></tr>';
                    }
                    echo '</table>';
                }
            }
            if(!$confirm) {
                echo '<h4>Aucun championnat en cours :/</h4>';
            }
        }
        echo '</div>';
        // Championships Members
        echo '<div style="background-color: #FFF; padding: 10px; margin-bottom: 10px; margin-right: 10px;">';
        echo '<h3>Classement des championnat (membres)</h3>';
        if($championships && !empty($championships)) {
            $confirm = false;
            foreach($championships as $championship) {
                $status = championship_status_manager::getChampionshipStatusByChampionshipId((int)$championship->id);
                if($status) {
                    $confirm = true;
                    $members = championship_display::getChampionshipMembersMembersPoints((int)$championship->id, 0, true);
                    echo '<table class="widefat fixed" cellspacing="0">';
                    echo '<tr><th>' . $championship->nom . '</th></tr>';
                    echo '<tr>' .
                    '<th class="manage-column column-columnname " scope="col">Place</th>' .
                    '<th class="manage-column column-columnname " scope="col">Id</th>' .
                    '<th class="manage-column column-columnname " scope="col">Num Adhérent</th>' .
                    '<th class="manage-column column-columnname " scope="col">Nom</th>' .
                    '<th class="manage-column column-columnname " scope="col">Prenom</th>' .
                    '<th class="manage-column column-columnname " scope="col">Club</th>' .
                    '<th class="manage-column column-columnname " scope="col">Points</th>' .
                    '<th class="manage-column column-columnname " scope="col">Profil</th></tr>';
                    if($members) {
                        $max_length = count($members) > 3 ? 3 : count($members);
                        for($i = 0; $i < $max_length; $i++) {
                            $member = $members[$i];
                            $selected_member= $members[$i]->member;
                            $selected_member_id = $selected_member ? $selected_member->id : '0';
                            echo '<tr style="border:1px solid #000">';
                            echo '<td>' . ($i+1) . '</td>';
                            echo '<td>' . $selected_member->id . '</td>';
                            echo '<td>' . $selected_member->adh . '</td>';
                            echo '<td>' . $selected_member->nom . '</td>';
                            echo '<td>' . $selected_member->prenom . '</td>';
                            if($selected_member->club) {
                                echo '<td><a title="Voir membres participants" href="?page=club_championship_db&d_members_members=' . $championship->id . '&club=' . $member->club->id . '">' . ucfirst($member->club->nom) . '</a></td>';
                            }
                            else if(!$member->has_club) {
                                echo '<td>Aucun club</td>';
                            }
                            else {
                                echo '<td>Club inconnu</td>';
                            }
                            echo '<td>' . $member->pts . '</td>';
                            echo '<td><a title="Voir profil" href="?page=club_member_db&profil=' . $selected_member->id . '">Voir</a></td>';
                            echo '</tr>';
                        }
                    }
                    else {
                        echo '<tr><td>Aucun membre :/</td></tr>';
                    }
                    echo '</table>';
                }
            }
            if(!$confirm) {
                echo '<h4>Aucun championnat en cours :/</h4>';
            }
        }
        else {
            echo '<h4>Aucun championnat en cours :/</h4>';
        }
        echo '</div>';
    }
}