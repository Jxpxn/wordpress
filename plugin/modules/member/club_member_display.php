<?php
class club_member_display {
    public static function displayTable() {
        $clubs = club_manager::getClubsWithName();
        echo "<h1>".get_admin_page_title()."</h1>";
        echo '<form method="post" id="club_member_form">';
        echo '<table class="widefat fixed" cellspacing="0">';
        echo '<input type="hidden" name="type" value="delete">';
        echo '<tr><th class="manage-column column-columnname " scope="col">Select</th>' .
            '<th class="manage-column column-columnname " scope="col">Id</th>' .
            '<th class="manage-column column-columnname " scope="col">Num Adhérent</th>' .
            '<th class="manage-column column-columnname " scope="col">Nom</th>' .
            '<th class="manage-column column-columnname " scope="col">Prenom</th>' .
            '<th class="manage-column column-columnname " scope="col">Email</th>' .
            '<th class="manage-column column-columnname " scope="col">Téléphone</th>' .
            '<th class="manage-column column-columnname " scope="col">Code Postal</th>' .
            '<th class="manage-column column-columnname " scope="col">Adresse</th>' .
            '<th class="manage-column column-columnname " scope="col">Club</th>' .
            '<th class="manage-column column-columnname " scope="col">Participant</th>' .
            '<th class="manage-column column-columnname " scope="col">Profil</th></tr>';
        foreach (club_member_manager::getClubsMember() as $line) {
            echo '<tr style="border:1px solid #000">';
            echo '<td><input class="widefat" type="checkbox" name="selected_member_' . $line->id . '" value="y" /></td>';
            echo '<td>' . $line->id . '</td>';
            echo '<td>' . $line->adh . '</td>';
            echo '<td>' . ucfirst($line->nom) . '</td>';
            echo '<td>' . ucfirst($line->prenom) . '</td>';
            echo '<td>' . $line->email . '</td>';
            echo '<td>' . $line->tel . '</td>';
            echo '<td>' . $line->cp . '</td>';
            echo '<td>' . $line->adresse . '</td>';
            if(mi_array_utils::arrayContain($clubs, (string)$line->club, true, true)) {
                echo '<td>';
                echo '<a title="Voir le club" href="?page=club_db&profil=' . $line->club . '">' . $clubs[$line->club] . '</a>';
                echo '</td>';
            }
            else if($line->club == 0) {
                echo '<td>Aucun club</td>';
            }
            else {
                echo '<td>Club inconnu</td>';
            }
            echo '<td>' . ($line->participant == 1 ? 'Oui' : 'Non') . '</td>';
            echo '<td><a title="Voir profil" href="?page=club_member_db&profil=' . $line->id . '">Voir</a></td>';
            echo '<td><a title="Editer membre" href="?page=club_member_db&edit=' . $line->id . '">✏️</a></td>';
            echo '</tr>';
        }
        echo '<tr><th class="manage-column column-columnname " scope="col"><label onclick="document.getElementById(`club_member_form`).submit();">Supprimer</label></th></tr>';
        echo '</table>';
        echo '</form>';
    }

    public static function displayProfil(string $member_id) {
        if(is_numeric($member_id)) {
            $member_id_int = (int)$member_id;
            $member = club_member_manager::getClubMemberById($member_id_int);
            if($member) {
                echo '<h1>Membre: ' .  ucfirst($member->nom) . ' ' . ucfirst($member->prenom) .' <a title="Editer membre" style="text-decoration: none;" href="?page=club_member_db&edit=' . $member_id .'">✏️</a></h1>';
                echo '<p>';
                echo 'n°' . $member->id . '<br>';
                echo 'Email: ' . $member->email . '<br>';
                echo 'Adresse: ' . $member->adresse . '<br>';
                echo 'Telephone: ' . $member->tel;
                $club = club_manager::getClubById((int)$member->club);
                echo '<h4>Club: ';
                if($club) {
                    echo '<a title="Voir club" href="?page=club_db&profil=' . $member->club . '">' . $club->nom . '</a>';
                }
                else if($member->club == 0) {
                    echo 'Aucun club';
                }
                else {
                    echo 'Club inconnu';
                }
                echo '</h4>';
            }
        }
    }

    public static function displayAdder() {
        if(mi_array_utils::containKeyPair($_POST, "add", "type")) {
            if(club_member_manager::registerMember($_POST)) {
                mi_utils::adminRedirection('club_member_db');
                return;
            }
            else {
                echo "<h4>Une erreur est survenu lors de l'inscription du membre :/</h4>";
            }
        }

        echo "<h1>".get_admin_page_title()."</h1>";

        echo '<form action="" method="post" style="padding-right: 15px;">';
        echo '<input type="hidden" name="type" value="add">';
        echo '<p>';
        echo '<label for="nom">Nom :</label>';
        echo '<input class="widefat" id="nom" name="nom" type="text" value="" />';
        echo '</p>';
        echo '<p>';
        echo '<label for="prenom">Prenom :</label>';
        echo '<input class="widefat" id="prenom" name="prenom" type="text" value="" />';
        echo '</p>';
        echo '<p>';
        echo '<label for="email">Email :</label>';
        echo '<input class="widefat" id="email" name="email" type="text" value="" />';
        echo '</p>';
        echo '<p>';
        echo '<label for="tel">Tel :</label>';
        echo '<input class="widefat" id="tel" name="tel" type="text" value="" />';
        echo '</p>';
        echo '<p>';
        echo '<label for="cp">Code Postal :</label>';
        echo '<input class="widefat" id="cp" name="cp" type="text" value="" />';
        echo '</p>';
        echo '<p>';
        echo '<label for="adresse">Adresse :</label>';
        echo '<input class="widefat" id="adresse" name="adresse" type="text" value="" />';
        echo '</p>';
        echo '<p>';
        echo '<label for="adh">Numéro adhérent :</label>';
        echo '<input class="widefat" id="adh" name="adh" type="text" value=""/>';
        echo '</p>';
        echo '<p>';
        echo '<label for="club">Club :</label>';
        echo '<select name="club" style="margin-left: 15px;">';
        echo '<option value="0">Aucun</option>';
        foreach(club_manager::getClubsWithName() as $key => $value) {
            echo '<option value="' . $key . '">' . $value . '</option>';
        }
        echo '</select>';
        echo '<p>';
        echo '<label for="participant" style="margin-right: 5px;">Participe :</label>';
        echo '<input class="widefat" id="participant" name="participant" type="checkbox" value="1" />';
        echo '</p>';
        echo '</p>';
        echo '<p><input type="submit" value="Enregister"></p>';
        echo '</form>';
    }

    public static function displayDelete(array $clubs) {
        $todelete = [];
        foreach($clubs as $key => $value) {
            if($value === 'y') {
                $parsed = explode('selected_member_', $key);
                $parsed = $parsed[count($parsed)-1];
                if(is_numeric($parsed)) {
                    array_push($todelete, $parsed);
                }
            }
        }
        foreach($todelete as $club) {
            club_member_manager::deleteClubMemberById((int)$club);
        }
        self::displayTable();
    }

    public static function displayConfEdit(string $member_id, array $args) {
        if(is_numeric($member_id)) {
            $member_id_int = (int)$member_id;
            $member = club_member_manager::getClubMemberById($member_id_int);
            if($member) {
                if(club_member_manager::updateClubMemberById($member_id_int, $args)) {
                    mi_utils::adminRedirection('club_member_db');
                    return;
                }
            }
            else {
                mi_utils::adminRedirection('club_member_db');
            }
        }
        mi_utils::adminRedirection('club_member_db', ['edit' => $member_id]);
    }

    public static function displayEditor(string $member_id) {
        if(is_numeric($member_id)) {
            $member_id_int = (int)$member_id;
            $member = club_member_manager::getClubMemberById($member_id_int);
            if($member) {
                echo "<h1>".get_admin_page_title()."</h1>";
                echo '<h2>Membre n°' . $member->id . '</h2>';
                echo '<form action="" method="post" style="padding-right: 15px;">';
                echo '<input type="hidden" name="conf_edit" value="' . $member->id . '">';
                echo '<p>';
                echo '<label for="nom">Nom :</label>';
                echo '<input class="widefat" id="nom" name="nom" type="text" value="'. $member->nom . '" />';
                echo '</p>';
                echo '<p>';
                echo '<label for="prenom">Prenom :</label>';
                echo '<input class="widefat" id="prenom" name="prenom" type="text" value="' . $member->prenom . '" />';
                echo '</p>';
                echo '<p>';
                echo '<label for="email">Email :</label>';
                echo '<input class="widefat" id="email" name="email" type="text" value="' . $member->email . '" />';
                echo '</p>';
                echo '<p>';
                echo '<label for="tel">Tel :</label>';
                echo '<input class="widefat" id="tel" name="tel" type="text" value="' . $member->tel . '" />';
                echo '</p>';
                echo '<p>';
                echo '<label for="cp">Code Postal :</label>';
                echo '<input class="widefat" id="cp" name="cp" type="text" value="' . $member->cp . '" />';
                echo '</p>';
                echo '<p>';
                echo '<label for="adresse">Adresse :</label>';
                echo '<input class="widefat" id="adresse" name="adresse" type="text" value="' . $member->adresse .'"/>';
                echo '</p>';
                echo '<p>';
                echo '<label for="adh">Numéro adhérent :</label>';
                echo '<input class="widefat" id="adh" name="adh" type="text" value="' . $member->adh . '"/>';
                echo '</p>';
                echo '<p>';
                echo '<label for="club">Club :</label>';
                echo '<select name="club" style="margin-left: 15px;">';
                echo '<option value="0">Aucun</option>';
                foreach(club_manager::getClubsWithName() as $key => $value) {
                    echo '<option ';
                    if($key == $member->club) {
                        echo 'selected="selected"';
                    }
                    echo ' value="' . $key;
                    echo '">' . $value . '</option>';
                }
                echo '</select>';
                echo '<p>';
                echo '<label for="participant" style="margin-right: 5px;">Participe :</label>';
                echo '<input class="widefat" id="participant" name="participant" type="checkbox" value="1" ' . ($member->participant != 0 ? 'checked="checked"' : '') . ' />';
                echo '</p>';
                echo '</p>';
                echo '<p><input type="submit" value="Modifier"></p>';
                echo '</form>';
            }
        }
        else {
            self::displayTable();
        }
    }
}