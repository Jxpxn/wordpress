<?php
class club_member_manager {
    public static function registerMember(array $member): bool {
        global $wpdb;
        if(mi_array_utils::arrayHasRequirements($member, mi_config::$club_member_requirements)) {
            $participant = $member['participant'] ? 1 : 0;
            $data = mi_array_utils::arrayExtract($member, mi_config::$club_member_requirements);
            $data['participant'] = $participant;
            if($data && !self::getClubMemberByEmail($data['email']) && mi_credentials_protection::adh($data['adh']) && mi_credentials_protection::cp($data['cp'])
             && !self::getClubMemberByNumAdh((int)$data['adh']) && 
            mi_credentials_protection::tel($data['tel']) && mi_credentials_protection::email($data['email'])) {
                $wpdb->insert($wpdb->prefix.mi_config::$mi_clubs_member_db_name, $data);
                return true;
            }
        }
        return false;
    }

    public static function getClubMemberbyClub(int $club_id) {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_member_db_name . " WHERE club=" . (string)$club_id . ";");
    }

    public static function getClubMemberByTel(string $tel) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_member_db_name . " WHERE tel='" . $tel . "';");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function getClubMemberByEmail(string $email) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_member_db_name . " WHERE email='" . $email . "';");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function getClubMemberById(int $id) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_member_db_name . " WHERE id=" . (string)$id . ";");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function getClubsMember() {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_member_db_name . ";");
    }

    public static function deleteClubMemberById(int $id): void {
        global $wpdb;
        $wpdb->delete($wpdb->prefix . mi_config::$mi_clubs_member_db_name, array("id" => $id));
    }

    public static function updateClubMemberById(int $id, array $member): bool {
        global $wpdb;
        if(mi_array_utils::arrayHasRequirements($member, mi_config::$club_member_requirements)) {
            $participant = $member['participant'] ? 1 : 0;
            $data = mi_array_utils::arrayExtract($member, mi_config::$club_member_requirements);
            $data['participant'] = $participant;

            if($data &&mi_credentials_protection::cp($data['cp']) &&
            mi_credentials_protection::adh($data['adh']) &&
            mi_credentials_protection::tel($data['tel']) && mi_credentials_protection::email($data['email'])) {
                $members = self::getClubMemberByNumAdh((int)$data['adh']);
                $check = true;
                if($members) {
                    $check = $members->id == $id;
                }
                if($check) {
                    $members = self::getClubMemberByEmail($data['email']);
                    if($members) {
                        $check = $members->id == $id;
                    }
                }
                if($check) {
                    $wpdb->update($wpdb->prefix.mi_config::$mi_clubs_member_db_name, $data, ['id' => $id]);
                    return true;
                }
            }
        }
        return false;
    }

    public static function kickClubMemberFromClubById(int $id): void {
        global $wpdb;
        $wpdb->update($wpdb->prefix . mi_config::$mi_clubs_member_db_name, array("club" => 0), ['id' => $id]);
    }

    public static function updateClubMemberClubById(int $id, int $club_id) {
        global $wpdb;
        $wpdb->update($wpdb->prefix.mi_config::$mi_clubs_member_db_name, ['club' => $club_id], ['id' => $id]);
    }

    public static function getClubMembersNumber(int $club_id): int {
        global $wpdb;
        $output = 0;
        $result = $wpdb->get_results("SELECT COUNT(`id`) AS total FROM " . $wpdb->prefix . mi_config::$mi_clubs_member_db_name . " WHERE club=" . (string)$club_id . ";");
        if($result != null && !empty($result)) {
            $output = $result[0]->total;
        }
        return $output;
    }

    public static function getClubMemberByNumAdh(int $num_adh) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_member_db_name . " WHERE adh=" . (string)$num_adh . ";");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }
}
