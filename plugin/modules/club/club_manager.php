<?php
class club_manager {
    public static function registerClub(array $club): bool {
        global $wpdb;
        if(mi_array_utils::arrayHasRequirements($club, mi_config::$club_requirements)) {
            $participant = $club['participant'] != 0 ? 1 : 0;
            $data = mi_array_utils::arrayExtract($club, mi_config::$club_requirements);
            $data['participant'] = $participant;
            if(!self::getClubByEmail($data['email']) && !self::getClubByName($data['nom']) && mi_credentials_protection::cp($data['cp'])) {
                $wpdb->insert($wpdb->prefix.mi_config::$mi_clubs_db_name, $data);
                return true;
            }
        }
        return false;
    }

    public static function getClubByName(string $name) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_db_name . " WHERE nom='" . $name . "';");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function getClubByEmail(string $email) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_db_name . " WHERE email='" . $email . "';");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function getClubById(int $id) {
        global $wpdb;
        $output = null;
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_db_name . " WHERE id=" . (string)$id . ";");
        if($result != null && !empty($result)) {
            $output = $result[0];
        }
        return $output;
    }

    public static function deleteClubsById(int $id): void {
        global $wpdb;
        $wpdb->delete($wpdb->prefix . mi_config::$mi_clubs_db_name, array("id" => $id));
    }

    public static function getClubsWithName(): array {
        global $wpdb;
        $output = [];
        $result = $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_db_name . ";");
        foreach($result as $line) {
            $output[(string)$line->id] = $line->nom;
        }
        return $output;
    }

    public static function getClubs() {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_db_name . ";");
    }

    public static function updateClubById(int $id, array $club): bool {
        global $wpdb;
        if(mi_array_utils::arrayHasRequirements($club, mi_config::$club_requirements)) {
            $participant = mi_array_utils::arrayContain($club, "participant", true) ? 1 : 0;
            $data = mi_array_utils::arrayExtract($club, mi_config::$club_requirements);
            $data['participant'] = $participant;
            if($data && mi_credentials_protection::cp($data['cp'])) {
                $wpdb->update($wpdb->prefix.mi_config::$mi_clubs_db_name, $data, ['id' => $id]);
                return true;
            }
        }
        return false;
    }

    public static function updateClubCategoryById(int $id, int $category_id) {
        global $wpdb;
        $wpdb->update($wpdb->prefix.mi_config::$mi_clubs_db_name, ['ddp' => $category_id], ['id' => $id]);
    }

    public static function getClubsByCategory(int $id) {
        global $wpdb;
        return $wpdb->get_results("SELECT * FROM " . $wpdb->prefix . mi_config::$mi_clubs_db_name . " WHERE ddp='" . (string)$id . "';");
    }
}
