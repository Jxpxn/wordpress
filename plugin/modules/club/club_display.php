<?php
class club_display {
    public static function displayTable()
    {
        echo "<h1>".get_admin_page_title()."</h1>";
        echo "<form method='post' id='club_db_form'>";
        echo '<input type="hidden" name="type" value="delete">';
        echo '<table class="widefat fixed" cellspacing="0">';
        echo '<tr><th class="manage-column column-columnname " scope="col">Select</th>' .
            '<th class="manage-column column-columnname " scope="col">Id</th>' .
            '<th class="manage-column column-columnname " scope="col">Nom</th>' .
            '<th class="manage-column column-columnname " scope="col">Code Postal</th>' .
            '<th class="manage-column column-columnname " scope="col">Adresse</th>' .
            '<th class="manage-column column-columnname " scope="col">Email</th>' .
            '<th class="manage-column column-columnname " scope="col">Tel</th>' .
            '<th class="manage-column column-columnname " scope="col">Participe</th>' .
            '<th class="manage-column column-columnname " scope="col">Profil</th></tr>';
        foreach (club_manager::getClubs() as $line) {
            echo '<tr style="border:1px solid #000">';
            echo '<td><input class="widefat" type="checkbox" name="selected_club_' . $line->id . '" value="y" /></td>';
            echo '<td>' . $line->id . '</td>';
            echo '<td>' . $line->nom . '</td>';
            echo '<td>' . $line->cp . '</td>';
            echo '<td>' . $line->adresse . '</td>';
            echo '<td>' . $line->email . '</td>';
            echo '<td>' . $line->tel . '</td>';
            echo '<td>' . ($line->participant == 1 ? 'Oui' : 'Non') . '</td>';
            echo '<td><a title="Voir profil" href="?page=club_db&profil=' . $line->id . '">Voir</a></td>';
            echo '<td><a title="Editer club" href="?page=club_db&edit=' . $line->id . '">✏️</a></td>';
            echo '</tr>';
        }
        echo '<tr><th class="manage-column column-columnname " scope="col"><label onclick="document.getElementById(`club_db_form`).submit();">Supprimer</label></th></tr>';
        echo '</table>';
        echo '</form>';
    }

    public static function displayProfil(string $club_id): void {
        if(is_numeric($club_id)) {
            $club_id_num = (int)$club_id;
            $club = club_manager::getClubById($club_id_num);
            if($club) {
                $members = club_member_manager::getClubMemberbyClub($club_id_num);
                $members_count = $members ? count($members) : 0;
                $championship_member = championship_members_manager::getChampionshipMembersByClubId($club_id_num);
                $championship_member_count = $championship_member ? count($championship_member) : 0;
                echo '<h1>Club: ' . $club->nom . ' <a title="Editer club" style="text-decoration: none;" href="?page=club_db&edit=' . $club_id .'">✏️</a></h1>';
                echo '<p>';
                echo 'n°' . $club->id . '<br>';
                echo 'Email: ' . $club->email . '<br>';
                echo 'Adresse: ' . $club->adresse . '<br>';
                echo 'Telephone: ' . $club->tel . '<br>';
                echo 'Participant: ' . ($club->participant == 1 ? '✔️' : '❌');
                echo '</p>';
                echo '<p>';
                echo '<h3 style="padding: 0; margin: 0;">Domaine de prédilection:</h3>';
                $category_name = 'Catégorie inconnue';
                $category = club_category_manager::getClubCategoryById((int)$club->ddp);
                if($category) {
                    $category_name = $category->nom;
                }
                else if($club->ddp == 0) {
                    $category_name = 'Aucune catégorie';
                }
                echo $category_name . '</p>';
                echo '<h4><a href="?page=club_db&members=' . $club_id . '">' . ($members_count > 0 ? 'Liste des membres (' . $members_count . ')' : 'Aucun membre') . '</a></h4>';
                echo '<h4><a href="?page=club_db&championship=' . $club_id . '">' . ($championship_member_count > 0 ? 'Liste des championnats (' . $championship_member_count . ')' : 'Aucun championnat') . '</a></h4>';
            }
        }
    }

    public static function displayDelete(array $clubs) {
        $todelete = [];
        foreach($clubs as $key => $value) {
            if($value === 'y') {
                $parsed = explode('selected_club_', $key);
                $parsed = $parsed[count($parsed)-1];
                if(is_numeric($parsed)) {
                    array_push($todelete, $parsed);
                }
            }
        }
        foreach($todelete as $club) {
            $members = club_member_manager::getClubMemberbyClub((int)$club);
            if(!empty($members)) {
                foreach($members as $member) {
                    club_member_manager::updateClubMemberClubById($member->id, 0);
                }
            }
            club_manager::deleteClubsById((int)$club);
        }
        self::displayTable();
    }

    public static function displayKick(array $clubs) {
        $todelete = [];
        $club_id = mi_array_utils::arrayContain($clubs, 'club', true, true) ? $clubs['club'] : '0';
        foreach($clubs as $key => $value) {
            if($value === 'y') {
                $parsed = explode('selected_member_', $key);
                $parsed = $parsed[count($parsed)-1];
                if(is_numeric($parsed)) {
                    if($club_id == 0) {
                        $member = club_member_manager::getClubMemberById((int)$parsed);
                        if($member) {
                            $club_id = $member->club;
                        }
                    }
                    array_push($todelete, $parsed);
                }
            }
        }
        foreach($todelete as $club) {
            club_member_manager::kickClubMemberFromClubById((int)$club);
        }
        self::displayMembers($club_id);
    }

    public static function displayMembers(string $club_id) {
        if(is_numeric($club_id)) {
            $club_id_num = (int)$club_id;
            $club = club_manager::getClubById($club_id_num);
            if($club) {
                $members = club_member_manager::getClubMemberbyClub($club_id_num);
                $members_count = $members ? count($members) : 0;
                echo '<h1>Club: ' .  $club->nom .'</h1>';
                echo '<h2>Membres (' . (string)$members_count .')</h2>';
                if($members_count > 0) {
                    echo '<form method="post" id="club_db_form">';
                    echo '<table class="widefat fixed" cellspacing="0">';
                    echo '<input type="hidden" name="type" value="kick">';
                    echo '<input type="hidden" name="club" value="' . $club_id . '"';
                    echo '<tr><th class="manage-column column-columnname " scope="col">Select</th>' .
                        '<th class="manage-column column-columnname " scope="col">Id</th>' .
                        '<th class="manage-column column-columnname " scope="col">Nom</th>' .
                        '<th class="manage-column column-columnname " scope="col">Prenom</th>' .
                        '<th class="manage-column column-columnname " scope="col">Profil</th></tr>';
                    foreach ($members as $line) {
                        echo '<tr style="border:1px solid #000">';
                        echo '<td><input class="widefat" type="checkbox" name="selected_member_' . $line->id . '" value="y" /></td>';
                        echo '<td>' . $line->id . '</td>';
                        echo '<td>' . ucfirst($line->nom) . '</td>';
                        echo '<td>' . ucfirst($line->prenom) . '</td>';
                        echo '<td><a title="Voir profil" href="?page=club_member_db&profil=' . $line->id . '">Voir</a></td>';
                        echo '</tr>';
                    }
                    echo '<tr><th class="manage-column column-columnname " scope="col"><label onclick="document.getElementById(`club_db_form`).submit();">Ejecter</label></th></tr>';
                    echo '</table>';
                    echo '</form>';
                }
                else {
                    echo '<h4>Ce club n' . "'" . 'a aucun membre :/</h4>';
                }
            }
        }
    }

    public static function displayKickChampionships(array $args) {
        $todelete = [];
        $club_id = mi_array_utils::arrayContain($args, 'club', true, true) ? $args['club'] : '0';
        foreach($args as $key => $value) {
            if($value === 'y') {
                $parsed = explode('selected_championship_', $key);
                $parsed = $parsed[count($parsed)-1];
                if(is_numeric($parsed)) {
                    if($club_id == 0) {
                        $member = championship_manager::getChampionshipById((int)$parsed);
                        if($member) {
                            $club_id = $member->club;
                        }
                    }
                    array_push($todelete, $parsed);
                }
            }
        }
        if(is_numeric($club_id)) {
            foreach($todelete as $club) {          
                championship_members_manager::deleteChampionshipMembersByClubIdAndChampionshipId((int)$club_id, (int)$club);
            }
        }
        mi_utils::adminRedirection("club_db", ['championship' => $club_id]);
    }

    public static function displayChampionships(string $club_id) {
        if(is_numeric($club_id)) {
            $club_id_num = (int)$club_id;
            $club = club_manager::getClubById($club_id_num);
            if($club) {
                $members = championship_members_manager::getChampionshipMembersByClubId($club_id_num);
                $members_count = $members ? count($members) : 0;
                echo '<h1>Club: ' .  $club->nom .'</h1>';
                echo '<h2>Championnats (' . (string)$members_count .')</h2>';
                if($members_count > 0) {
                    $championship = championship_manager::getChampionships();
                    $list_ = [];
                    if($championship) {
                        foreach($championship as $championship_) {
                            $flag = false;
                            foreach($members as $value) {
                                if($value->championship_id == $championship_->id) {
                                    $flag = true;
                                    break;
                                }
                            }
                            if($flag) {
                                array_push($list_, $championship_);
                            }
                        }
                    }
                    if(!empty($list_)) {
                        echo '<form method="post" id="club_db_form">';
                        echo '<table class="widefat fixed" cellspacing="0">';
                        echo '<input type="hidden" name="type" value="kick_championship">';
                        echo '<input type="hidden" name="club" value="' . $club_id . '"';
                        echo '<tr><th class="manage-column column-columnname " scope="col">Select</th>' .
                            '<th class="manage-column column-columnname " scope="col">Id</th>' .
                            '<th class="manage-column column-columnname " scope="col">Nom</th>' .
                            '<th class="manage-column column-columnname " scope="col">Membres</th></tr>';
                        foreach ($list_ as $line) {
                            echo '<tr style="border:1px solid #000">';
                            echo '<td><input class="widefat" type="checkbox" name="selected_championship_' . $line->id . '" value="y" /></td>';
                            echo '<td>' . $line->id . '</td>';
                            echo '<td>' . ucfirst($line->nom) . '</td>';
                            echo '<td><a title="Voir profil" href="?page=club_championship_db&members=' . $line->id . '">Voir</a></td>';
                            echo '<td><a title="Voir championnat" href="?page=club_championship_db&status=' . $line->id . '">👁️</a></td>';
                            echo '</tr>';
                        }
                        echo '<tr><th class="manage-column column-columnname " scope="col"><label onclick="document.getElementById(`club_db_form`).submit();">Quitter championnats</label></th></tr>';
                        echo '</table>';
                        echo '</form>';
                    }
                    else {
                        echo '<h4>Ce club ne participe à aucun championnat :/</h4>';
                    }
                }
                else {
                    echo '<h4>Ce club ne participe à aucun championnat :/</h4>';
                }
            }
        }
    }

    public static function displayConfEdit(string $club_id, array $args) {
        if(is_numeric($club_id)) {
            $club_id_int = (int)$club_id;
            $club = club_manager::getClubById($club_id_int);
            if($club) {
                if(club_manager::updateClubById($club_id_int, $args)) {
                    mi_utils::adminRedirection('club_db');
                    return;
                }
            }
        }
        mi_utils::adminRedirection('club_db', ['edit' => $club_id]);
    }

    public static function displayEditor(string $club_id) {
        if(is_numeric($club_id)) {
            $club_id_int = (int)$club_id;
            $club = club_manager::getClubById($club_id_int);
            if($club) {
                echo "<h1>".get_admin_page_title()."</h1>";
                echo '<h2>Club n°' . $club->id . '</h2>';
                echo '<form action="" method="post" style="padding-right: 15px;">';
                echo '<input type="hidden" name="conf_edit" value="' . $club->id . '">';
                echo '<p>';
                echo '<label for="nom">Nom :</label>';
                echo '<input class="widefat" id="nom" name="nom" type="text" value="' . $club->nom . '" />';
                echo '</p>';
                echo '<p>';
                echo '<label for="cp">Code Postal :</label>';
                echo '<input class="widefat" id="cp" name="cp" type="text" value="' . $club->cp . '" />';
                echo '</p>';
                echo '<p>';
                echo '<label for="adresse">Adresse :</label>';
                echo '<input class="widefat" id="adresse" name="adresse" type="text" value="' . $club->adresse . '" />';
                echo '</p>';
                echo '<p>';
                echo '<label for="email">Email :</label>';
                echo '<input class="widefat" id="email" name="email" type="text" value="' . $club->email . '" />';
                echo '</p>';
                echo '<p>';
                echo '<label for="tel">Tel :</label>';
                echo '<input class="widefat" id="tel" name="tel" type="text" value="' . $club->tel .  '" />';
                echo '</p>';
                echo '<p>';
                echo '<label for="ddp">Domaine de prédilection :</label>';
                echo '<select id="dpp" name="ddp">';
                echo '<option value="0">Aucun</option>';
                foreach(club_category_manager::getClubsCategory() as $category) {
                    echo '<option ';
                    if($category->id == $club->ddp) {
                        echo 'selected="selected"';
                    }
                    echo ' value="' . $category->id;
                    echo '">' . $category->nom . '</option>';
                }
                echo '</select>';
                echo '</p>';
                echo '<p>';
                echo '<label for="participant" style="margin-right: 5px;">Participe :</label>';
                echo '<input class="widefat" id="participant" name="participant" type="checkbox" value="1" ' . ($club->participant == 1 ? 'checked="checked"' : '') . ' />';
                echo '</p>';
                echo '<p><input type="submit" value="Enregister"></p>';
                echo '</form>';
            }
        }
        else {
            self::displayClubTable();
        }
    }

    public static function displayAdder() {
        if(mi_array_utils::containKeyPair($_POST, "add", "type")) {
            if(club_manager::registerClub($_POST)) {
                mi_utils::adminRedirection('club_db');
                return;
            }
            else {
                echo "<h4>Une erreur est survenu lors de l'inscription du club :/</h4>";
            }
        }

        echo "<h1>".get_admin_page_title()."</h1>";

        echo '<form action="" method="post" style="padding-right: 15px;">';
        echo '<input type="hidden" name="type" value="add">';
        echo '<p>';
        echo '<label for="nom">Nom :</label>';
        echo '<input class="widefat" id="nom" name="nom" type="text" value="" />';
        echo '</p>';
        echo '<p>';
        echo '<label for="cp">Code Postal :</label>';
        echo '<input class="widefat" id="cp" name="cp" type="text" value="" />';
        echo '</p>';
        echo '<p>';
        echo '<label for="adresse">Adresse :</label>';
        echo '<input class="widefat" id="adresse" name="adresse" type="text" value="" />';
        echo '</p>';
        echo '<p>';
        echo '<label for="email">Email :</label>';
        echo '<input class="widefat" id="email" name="email" type="text" value="" />';
        echo '</p>';
        echo '<p>';
        echo '<label for="tel">Tel :</label>';
        echo '<input class="widefat" id="tel" name="tel" type="text" value="" />';
        echo '</p>';
        echo '<p>';
        echo '<label for="ddp">Domaine de prédilection :</label>';
        echo '<select id="dpp" name="ddp">';
        echo '<option value="0">Aucun</option>';
        foreach(club_category_manager::getClubsCategory() as $category) {
            echo '<option value="' . $category->id . '">' . $category->nom . '</option>';
        }
        echo '</select>';
        echo '</p>';
        echo '<p>';
        echo '<label for="participant" style="margin-right: 5px;">Participe :</label>';
        echo '<input class="widefat" id="participant" name="participant" type="checkbox" value="1" />';
        echo '</p>';
        echo '<p><input type="submit" value="Enregister"></p>';
        echo '</form>';
    }
}