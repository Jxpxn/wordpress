<!DOCTYPE html>
<html lang="fr" style="margin: 0;" class="<?php 
    if(!defined('mi_occitanie_no_background_header')) {
        echo 'with-bg';
    }  ?>">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php echo get_bloginfo('name');?></title>
    <?php wp_head(); ?>
</head>
<body>
<div class="container" style="flex-grow: 1;">
    <h1 class="text-light text-center main-title"><?php echo get_bloginfo('name'); ?></h1>
    <nav class="navbar navbar-expand-lg navbar-light">
    <?php


        wp_nav_menu(array(
        'theme_location' => 'menu-top',
        'container'       => 'div',
        'container_class' => 'collapse navbar-collapse',
        'container_id'    => 'navbarSupportedContent',
        'menu_class' => 'navbar-nav mr-auto',
        'menu_id' => ' ',
        'walker' => new mi_menu_walker(),
        ));
        ?>
    </nav>