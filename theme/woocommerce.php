<?php 
DEFINE('mi_occitanie_no_background_header', true);
get_header(); ?>
<div class="home-post" style="width: inherit; margin-top: 5px;">
<div id="main" class="row">
<div id="content" class="col-lg-12 col-sm-6 col-md-6 col-xs-12">
<?php woocommerce_content(); ?>
</div>
</div>
</div>
<?php get_footer(); ?>