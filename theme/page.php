<?php
get_header();
?>
<div class="container" style="display: flex;">
    <div class="row" style="flex-basis: 66.666666%;">
        <div class="container mt-4" id="article">
            <?php
                if( have_posts() ) { 
                    echo '<h3 style="padding-bottom: 1%;">Dernier Posts:</h3>';
                    while ( have_posts() ) {
                    ?>
                    <div class="home-post">
                    <?php
                    the_post();
                    the_title();
                    echo '<hr>';
                    the_excerpt();
                    ?>
                    </div>
                    <?php
                    }
                }
                else {
                    echo '<h4>Aucun post trouver :/</h4>';
                }
            ?>
        </div>
    </div>
        <?php if ( is_active_sidebar( 'footer-1' ) ) { ?>
            <div class="home-post" style="flex-basis: 33.3333333333%; margin-top: 10px; margin-bottom: 10px; display: flex; justify-content: center; flex-direction: column; text-align: center;">
                <hr>
                <?php
                    dynamic_sidebar( 'footer-1' );
                }
                ?>
                <hr>
            </div>
        </div>
<?php
get_footer();