<?php
class mi_menu_walker extends Walker_Nav_Menu
{
    public function start_lvl(&$output, $depth = 0, $args = array())
    {
            $output .= '<ul class="dropdown-menu">';
    }

    function start_el(&$output, $item, $depth=0, $args=array(), $id = 0)
    {

        $title = $item->title;
        $description = $item->description;
        $permalink = $item->url;

        if($permalink != '#'){
            if($depth == 1){
               $output .= '';
            } else {

                $output .= "<li class='nav-item'>";
            }
        } else {
            $output .= "<li class='nav-item dropdown'>";
        }

        if( $permalink && $permalink != '#' ) {
            if($depth == 1){
                $output .= '<a href="' . $permalink . '" class="dropdown-item">';
            } else {
                $output .= '<a href="' . $permalink . '" class="nav-link">';
            }
        } else {
                $output .= '<a href="' . $permalink .
                    '" class="nav-link dropdown-toggle" data-toggle="dropdown">';
        }

        $output .= $title;
        if( $description != '' && $depth == 0 ) {
            $output .= '<small class="description">' . $description . '</small>';
        }
        $output .= '</a>';
    }

    public function end_el(&$output, $item, $depth = 0, $args = array())
    {
        if($depth == 1){
            $output .='';
        }
    }

}

function mi_occitanie_load_styles(): void {
    wp_register_style('bootstrap_style', 'https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css', array(),true);
    wp_enqueue_style('bootstrap_style');
    wp_register_style('main_style', get_template_directory_uri().'/style.css', array(), true);
    wp_enqueue_style('main_style');

}
function mi_occitanie_load_menu()
{
    register_nav_menus(
        array(
            'menu-top' => __('Top Menu'),
        )
    );
}
function mi_occitanie_setup() {
    remove_action('wp_head', '_admin_bar_bump_cb');
    add_theme_support('post-thumbnails');
}
function customtheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}
function toy_widgets_init()
{
    if ( function_exists('register_sidebar') ) {
        register_sidebar(array(
            'name' => __('Top widget sidebar', 'toys'),
            'description' => __('Top widgets', 'toys'),
            'id' => 'footer-1',
            'before_widget' => '<div id="%1$s" class="widget %2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h5>',
            'after_title' => '</h5>',
        ));
    }
}

add_action( 'widgets_init', 'toy_widgets_init' );
add_action( 'after_setup_theme', 'customtheme_add_woocommerce_support' );
add_action('get_header', 'mi_occitanie_setup');
add_action('init', 'mi_occitanie_load_menu');
add_action('wp_enqueue_scripts', 'mi_occitanie_load_styles');